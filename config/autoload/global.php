<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Db\Adapter\Adapter;
use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Zend\ServiceManager\ServiceManager;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Config\Reader\Ini as ConfigIni;
use Zend\Session\Container;
use Zend\Cache;


class Globals{
    public static $db=null;
    public static $acl=null;
    public static $isLoggedIn=null;
    public static $user=null;
    public static $config=null;
    private static $lang = null;
    private static $langs = null;
     /** @var Zend\Cache */
    private static $_cache = null;
    
    public static function getDBConnection(){
            if(self::$db != null){
                return self::$db;
            }
             $config = Globals::getConfig();
           self::$db = new Adapter(array(
                'driver' => $config->db['adapter'],
                'database' => $config->db['dbname'],
                'username' => $config->db['username'],
                'password' => $config->db['password'],
                'hostname' => $config->db['host'],
                'port' => $config->db['port'],
                'charset' => $config->db['charset'],
                'driver_options'=>array(PDO::MYSQL_ATTR_INIT_COMMAND=>'SET NAMES UTF8;'),
        ));
           
        return self::$db;
    }
    
    public static function executeDBQuerry($querry){
       
        if(self::$db == null){
                self::getDBConnection();
            }
            
         $adapter=self::$db;
        $resultSet = $adapter->query($querry, $adapter::QUERY_MODE_EXECUTE);

        return($resultSet);
    }    

    public static function getACL(){
       if(self::$acl==null){
         self::$acl = new Acl(); 
         self::getDBConnection();
         $querry='Select * from role';
         $role=self::executeDBQuerry($querry);
         foreach($role as $k=>$p){
             self::$acl->addRole(new Role($p->id)) ;
          }         
         $querry='Select * from resource';
         $resource=self::executeDBQuerry($querry);
         $zrodla=array();
         foreach($resource as $i=>$w){
             self::$acl->addResource(new Resource($w->akcja)) ;
             $zrodla[$w->id]=$w->akcja;
          }        
        $querry='Select * from privileges';
        $privileges=self::executeDBQuerry($querry);
          
        foreach($privileges as $i=>$w){
           self::$acl->allow($w->id_role,$zrodla[$w->id_resource]);            
        } 
       }
       return self::$acl;
    }
    
    public static function ifLoggedIn(){
        $log= new  \Zend\Authentication\AuthenticationService();
        if($log->hasIdentity()){
            self::$isLoggedIn=true;            
        }else{
            self::$isLoggedIn=false;            
        }
         return self::$isLoggedIn;
    }
    public static function getLoggedIn(){
        $log= new  \Zend\Authentication\AuthenticationService();
        
        $querry='Select id, stan from users where username=\''.$log->getIdentity().'\' limit 1'; 
        $user=self::executeDBQuerry($querry);
        
        if($user->count()>0){
            self::$user=$user->current()->id;            
        }else{
            self::$user=false;            
        }
         return self::$user;
    }
    
    public static function getConfig(){
		if (self::$config != null) {
			return self::$config;
		}
		$reader = new ConfigIni();
                $config = $reader->fromFile('config/config.ini');
		
                self::$config = (object) $config;
                
		return self::$config;
	}
    public static function getLang(){
            
		$namespace = new Container();
                $lang = ($namespace->lang?$namespace->lang:'pl');
		
		self::$lang = $lang;
		
		return self::$lang;
        }
    public static function getLangs(){
        if(self::$langs==null){
            if(self::getConfig()->langs!=''){
                self::$langs = explode(";", self::$config->langs);
            }
        }
        return self::$langs;
    }
    public static function getCache(){
        if(self::$_cache != null){
            return self::$_cache;
        }

        self::$_cache = Cache\StorageFactory::factory(array(
            'adapter'=>array(
                'name'=>'Filesystem',
                'options'=>array(
                    'ttl'=>  self::getConfig()->cachelifetime,
                    'cache_dir'=> self::getConfig()->app_path.self::getConfig()->cachedir,
                ),
            ),
            'plugins'=>array(
                'serializer'
            )
        ));

        return self::$_cache;
    }
    public static function loadSeolinks()
    {
        $container = new Container('seolinks');
        $seolinkarray = $container->seolinks;      
            $seolinkarray = \Globals::executeDBQuerry("Select Id, Route, Action, Module, PageId, Seo".self::getLang()." as Seo from seo");
            if(isset($seolinkarray) && $seolinkarray->count()>0)
            {
                $seolinkarray = $seolinkarray->toArray();
                $container->seolinks = $seolinkarray;
            }                
        
        return $seolinkarray;
    }
}

return array(
    // ...
);
