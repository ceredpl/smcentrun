<?php
 
namespace Boxy\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Boxy\Form\Add as AddForm;
// Banner\Form\Add as AddForm,

class BoxyController extends AbstractActionController
{

    private $NazwaModulu = 'Boxy';
     
    public function listAction()
    {
       $return = array();   
        $cfg = $this->getServiceLocator()->get('ModuleManager')->getModule($this->NazwaModulu)->getConfig();
        $cfg = $cfg['cms_config'];        
        $lang=\Globals::getLang();
        $id = strip_tags($this->getEvent()->getRouteMatch()->getParam('id'));
        
        $naglowki=array();
        $sql = 'select Id';
        foreach ($cfg['ListFields'] as $key => $field) {
            $naglowki[$key]=$field;
            if($cfg['Pola'][$key]['Lang']){//sprawdzenie czy pole jest w językach
                $sql.=','.$key.$lang;
            }else{
                $sql.=','.$key;
            }
        }  
        if($cfg['ListType']=='list'){
        /*
         * Filtrowanie
         */
        
        /*
         * Przygotowanie sortowania 
         */
        
        $post = array_merge_recursive($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray());
        $searchsql = "";
        $sort = null;
        $order = null;
        $session = new \Zend\Session\Container($cfg['RouteName']);
        $search;
         
        /*
         * Wyszukiwanie
         */     
            $searchoffset = $session->offsetGet("search");    
            if(is_array($post) && count($post)>0 && isset($post['Search']) && $post['Search']!="")
            {
                $search = $post['Search'];
                $session->search = $post['Search'];
            }
            elseif(is_array($post) && count($post)>0 && $post['Search']=="")
            {
                $session->search = null;
                $search = null;
            }
            elseif($searchoffset!="empty")       
            {   //jest w zapytaniu - dodać do sesji
                $search = $session->search;                             
            }
            //var_dump($search);die();
             if($search)
            {
                if(count($cfg['Filter']['searchfield'])==1){
                    foreach ($cfg['Filter']['searchfield'] as $key => $searchword) {
                        $searchsql .= ' and '.$searchword.($cfg['Pola'][$searchword]['Lang']==true?\Globals::getLang():'').' like \'%'.$search.'%\'';
                    }
                }
                else
                {
                    $searchsql .= ' and( ';
                    foreach ($cfg['Filter']['searchfield'] as $key => $searchword) {
                        $searchsql .= $searchword.($cfg['Pola'][$searchword]['Lang']==true?\Globals::getLang():'').' like \'%'.$search.'%\'';
                        if($key!=count($cfg['Filter']['searchfield'])-1)
                        {
                            $searchsql .= ' or ';
                        }
                    }
                    $searchsql .= ' )';
                }
                $return['search'] = $search;
            }
            
            /*
            * Stronicowanie
            */
            $sortoffset = $session->offsetGet("sort");    
            
            if(is_array($post) && count($post)>0 && isset($post['sort']) && $post['sort']!="")
            {
                $sort = $post['sort'];
                $session->sort = $sort;
            }
            elseif(is_array($post) && count($post)>0 && $post['Search']=="")
            {
                $session->sort = null;
                $sort = null;
            }
            elseif($sortoffset!="empty")              
            {   //jest w zapytaniu - dodać do sesji
                $sort = $session->sort;
            }    
            
            if($sort && $sort!="empty")
            {
                $order.=" order by ".$sort;
                $return['sort'] = $sort;
            }
           /* if($post["sort"]!="empty")
            {
                $order.=" order by ".$post["sort"];
                $return['sort'] = $post["sort"];
            }*/
          
        $countSQL = 'Select count(*) as count from '.$cfg['Table'].' where 1=1 '.$searchsql;
        $count = \Globals::executeDBQuerry($countSQL);
        $page = $this->getEvent()->getRouteMatch()->getParam('page');        
        $offset = "limit ".$page*$cfg['Rowperpage'].",".$cfg['Rowperpage'];
        
        $ilosc = $count->current()->count;
        if($ilosc>$cfg['Rowperpage'])
        {             
            $return['page'] = $page;
            $pages = round($ilosc/$cfg['Rowperpage'],0,2);           
            $return['pages'] = $pages;
        }
      
        /*
         * Stronicowanie end
         */
       
            $return['naglowki']=$naglowki;

            $sql.= ' from '.$cfg['Table'].' where 1=1 ';

            if($cfg['Warunek']!=null){//sprawdzenie czy warunek w configu jest różny od null
                $sql.=" and ".$cfg['Warunek'];
            }           
            if($id>0 && $cfg['OuterField']!=''){//sprawdzenie czy wyświetlanie tylko wierszy o danym id                
                $sql.=" and ".$cfg['OuterField'].' ='.$id;                
            }
            $sql .= $searchsql;
            if($order!="")
            {
                $sql.=$order;
            }
            elseif($cfg['OrderBy']!=null){//sprawdzenie order by
                $sql.=" order by ".$cfg['OrderBy'];
            }else{
                $sql.=" order by Id asc ";
            }
            $sql.=" ".$offset;   
            $sql.=";";   
         
            
        $wiersze = \Globals::executeDBQuerry($sql);
        if($wiersze->count()>0){
            $return['wiersze']=$wiersze->toArray();        
        }

        
        }else if($cfg['ListType']=='tree'){
             $i=0;
        $sql = 'select Id, PrevId';
        foreach($cfg['Pola'] as $i=>$raw){
            if($raw['Visible']!=null){//sprawdzenie czy pole powinno być widoczne w liście
                if($raw['Lang']){//sprawdzenie czy pole jest w językach
                    $sql.=','.$i.$lang;
                }else{
                    $sql.=','.$i;
                }
                $naglowki[$i]=$raw['Nazwa'];
                $i++;
            }
        }        
        $return['naglowki']=$naglowki;
        
        $sql.= ' from '.$cfg['Table'];
        
           $return['tree']= $this->prepareTree(0,$cfg);
         
        }
       $return['config']=$cfg;
       
       if($id && $id>0){         
           $return['supermodule']=$id;
       }
       
          return $return;
    }
    
    public function deleteAction(){
        $response = $this->getResponse();
        $response->setStatusCode(200);  //!!! zatrzumuje rendering !!!
        $id=$this->getRequest()->getPost('id');
        $cfg = $this->getServiceLocator()->get('ModuleManager')->getModule($this->NazwaModulu)->getConfig();
        $cfg = $cfg['cms_config']; 
        $Item = new $cfg['ModelName'](0,$this->getServiceLocator());
        $Item->setId($id);        
        $Item->delete();    
        
        /*
         * Usuwanie linków seo
         */
        if(is_array($cfg['Seo'])){
            $delarray = array(
                'Module' => $cfg['RouteName'],
                'PageId' => $id
            );
            $seolink = new \Boxy\Model\Seo($delarray);  
            $seolink->delete();
        }
        $response->setContent(1);
        return $response;       
   }
   /*
    * Akcja zmiana wartości (Stan)
    */
   public function changevalueAction(){
            $response = $this->getResponse();
            $response->setStatusCode(200);  //!!! zatrzumuje rendering !!!
            $id=$this->getRequest()->getPost('id');
            $stateid=$this->getRequest()->getPost('stateid');
            $value=$this->getRequest()->getPost('value');
            
            $cfg = $this->getServiceLocator()->get('ModuleManager')->getModule($this->NazwaModulu)->getConfig();
            $cfg = $cfg['cms_config'];            
            $tmpArray=$cfg['Pola'][$value]['Values'];
            
            $stateid++;
            if($stateid>sizeof($tmpArray)-1){
                $stateid=0;
            }
            
           $querry='update '.$cfg['Table'].' set '.$value.'=\''.$stateid.'\' where id=\''.$id.'\'';
           
           \globals::executeDBQuerry($querry); 
           
            $response->setContent(1);
            return $response;       
   }  
   
   private function prepareTree($prevId=0,$cfg=null,$glebokosc=0){
       $Tree = false;
       $id = strip_tags($this->getEvent()->getRouteMatch()->getParam('id'));
      if($id>0){//sprawdzenie czy wyświetlanie tylko wierszy o danym id               
                    $tmpSQL=" and ".$cfg['OuterField'].' ='.$id;                
            }
   
          
       $fields="Id, PrevId";
            foreach ($cfg["ListFields"] as $k => $field) {                  
                if($cfg["Pola"][$k]["Lang"])
                {
                    $fields.=",".$k.strtoupper(\Globals::getLang())." as ".$k." ";
                }
                else
                {
                    $fields.=",".$k." ";
                }
            }
             
            $sql = 'select '.$fields.' from '.$cfg['Table'].' where PrevId='.$prevId.' '. (($id>0)?$tmpSQL:'') .' order by kolej asc;';           
            $items = \Globals::executeDBQuerry($sql);
            if($items->count()>0 && $glebokosc<$cfg['Glebokosc']){
            $Tree = array();
            foreach ($items as $key => $item) {         
                $Tree[$key]['item'] = $item;                
                $Tree[$key]['sub'] = $this->prepareTree($item->Id, $cfg, $glebokosc+1);
                $Tree[$key]['Glebkosc'] = $glebokosc;
            }
        }
            
       return $Tree;
   }

   /*
    * zmiana kolejności
    */
   public function changeTurnAction(){
            $response = $this->getResponse();
            $response->setStatusCode(200);  //!!! zatrzumuje rendering !!!
            $order=$this->getRequest()->getPost('order');
            
            $cfg = $this->getServiceLocator()->get('ModuleManager')->getModule($this->NazwaModulu)->getConfig();
            $cfg = $cfg['cms_config'];            
           //var_dump($order);die();
            foreach ($order as $key => $value) {
                $tmp=explode("-", $value);
                $querry='update '.$cfg['Table'].' set kolej=\''.($key+1).'\' where id=\''.$tmp[2].'\'';
                \globals::executeDBQuerry($querry); 
            }
            $response->setContent(1);
            return $response;       
   }
   
   
   public function addAction(){
      
            $form=new AddForm(null, $this->getServiceLocator());
            $form->get('submit')->setValue('Dodaj');
            $return['form'] = $form;
        
            $id = strip_tags($this->getEvent()->getRouteMatch()->getParam('id'));
            
            $cfg = $this->getServiceLocator()->get('ModuleManager')->getModule($this->NazwaModulu)->getConfig();
            $cfg = $cfg['cms_config'];   
            
            if ($this->flashMessenger()->setNamespace($cfg['RouteName'].'add')->hasErrorMessages()) {
                $return['error'] = implode("<br />", $this->flashMessenger()->setNamespace($cfg['RouteName'].'add')->getErrorMessages());
                
                if($this->flashMessenger()->setNamespace($cfg['RouteName'].'addform')->hasMessages()){
                    $messages = $this->flashMessenger()->setNamespace($cfg['RouteName'].'addform')->getMessages();
                    if(is_array($messages)){
                        $form->bind($messages[0]);
                    }
                } 
            }
            if ($this->flashMessenger()->setNamespace($cfg['RouteName'].'addinfo')->hasMessages()) {
                $return['message'] = implode("<br />", $this->flashMessenger()->setNamespace($cfg['RouteName'].'addinfo')->getMessages());
            }
            
            $return['config']=$cfg;
            $return['id']=$id;
            return $return;
   }
   public function dodajAction(){
      
            $viewModel = new \Zend\View\Model\ViewModel();
            $viewModel->setTerminal(true); //!!! wyłącza layout !!!

            $response = $this->getResponse();
            $response->setStatusCode(200);  //!!! zatrzumuje rendering !!!
            
            $cfg = $this->getServiceLocator()->get('ModuleManager')->getModule($this->NazwaModulu)->getConfig();
            $cfg = $cfg['cms_config'];
            
            $form = new AddForm(null, $this->getServiceLocator());//deklaracja AddForm();
            $post = array_merge_recursive($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray());//przechwycenie danych i plików z formularza
            $form->setData($post);//przekazanie danych do forma.
            $this->flashMessenger()->setNamespace($cfg['RouteName'].'addform')->addMessage($this->getRequest()->getPost());
            if($form->isValid()){
                
            $id = strip_tags($this->getEvent()->getRouteMatch()->getParam('id'));
                
                $data = $form->getData();
                $Item = new $cfg['ModelName'](0,$this->getServiceLocator());
                $err = false;
                         
                 foreach($cfg['Pola'] as $name => $field){    
                    if($field['Form']){
                     if($field['FormType']=='file')
                     {//obsługa pliku
                        if($data[$name]['name']!=''){
                            $validatorChain = new \Zend\Validator\ValidatorChain();
                            $tmpExt = explode(".", $data[$name]['name']);
                            if(is_array($tmpExt)){
                                $tmpExt = strtolower($tmpExt[count($tmpExt)-1]);
                            }

                            if(is_array($field['Mime'])){// mime type
                                if(!in_array($data[$name]['type'], $field['Mime'])){
                                    $this->flashMessenger()->setNamespace($cfg['RouteName'].'add')->addErrorMessage('Błąd! Plik '.$field['Name'].' posiada nieodpowiedni typ (mimetype).');
                                    $err =true;
                                }
                            }
                            if(is_array($field['Ext'])){//rozszerzenie 
                                if(!in_array($tmpExt, $field['Ext'])){
                                    $this->flashMessenger()->setNamespace($cfg['RouteName'].'add')->addErrorMessage('Błąd! Plik '.$field['Name'].' posiada nieodpowiednie rozszerzenie.');
                                    $err =true;
                                }
                            }
                            if($field['FileMaxSize']!=null){
                                $validatorChain->attach(
                                    new \Zend\Validator\File\Size($field['FileMaxSize'].'kB')
                                );
                            }
                            if($field['ImgMinSize']!=null){
                                $validatorChain->attach(
                                    new \Zend\Validator\File\ImageSize(array(
                                        'minWidth' => $field['ImgMinSize'],
                                        'minHeight' => $field['ImgMinSize']
                                        )
                                    )
                                );
                            }
                            if($field['ImgMaxSize']!=null){
                                $validatorChain->attach(
                                    new \Zend\Validator\File\ImageSize(array(
                                        'maxWidth' => $field['ImgMaxSize'],
                                        'maxHeight' => $field['ImgMaxSize']
                                        )
                                    )
                                );
                            }

                            if(!$validatorChain->isValid($data[$name]['tmp_name']) || $err){
                                //tutaj wypisanie errorów
                                unlink($data[$name]['tmp_name']);
                                //var_dump($validatorChain->getMessages());
                                foreach($validatorChain->getMessages() as $key => $message){
                                    switch($key){

                                        case 'fileSizeTooBig':
                                            $this->flashMessenger()->setNamespace($cfg['RouteName'].'add')->addErrorMessage('Błąd! Plik '.$field['Name'].' jest zbyt duży. Maksymalny rozmiar pliku to '.$field['FileMaxSize'].'kB.');
                                        break;
                                        case 'fileImageSizeWidthTooBig':
                                            $this->flashMessenger()->setNamespace($cfg['RouteName'].'add')->addErrorMessage('Błąd! Plik '.$field['Name'].' ma za dużą szerokość. Maksymalny wymiar pliku to '.$field['ImgMaxSize'].' x '.$field['ImgMaxSize'].' px');
                                        break;
                                        case 'fileImageSizeHeightTooBig':
                                            $this->flashMessenger()->setNamespace($cfg['RouteName'].'add')->addErrorMessage('Błąd! Plik '.$field['Name'].' ma za dużą wysokość. Maksymalny wymiar pliku to '.$field['ImgMaxSize'].' x '.$field['ImgMaxSize'].' px');
                                        break;
                                        case 'fileImageSizeWidthTooSmall':
                                            $this->flashMessenger()->setNamespace($cfg['RouteName'].'add')->addErrorMessage('Błąd! Plik '.$field['Name'].' ma za małą szerokość. Minimalny wymiar pliku to '.$field['ImgMinSize'].' x '.$field['ImgMinSize'].' px');
                                        break;
                                        case 'fileImageSizeHeightTooSmall':
                                            $this->flashMessenger()->setNamespace($cfg['RouteName'].'add')->addErrorMessage('Błąd! Plik '.$field['Name'].' ma za małą wysokość. Minimalny wymiar pliku to '.$field['ImgMinSize'].' x '.$field['ImgMinSize'].' px');
                                        break;
                                        case 'fileImageSizeNotDetected':
                                            $this->flashMessenger()->setNamespace($cfg['RouteName'].'add')->addErrorMessage('Błąd! Plik '.$field['Name'].' nie jest plikiem graficznym');
                                        break;
                                    }
                                }

                                return $this->redirect()->toRoute($cfg['RouteName'], array('action'=>'add'));
                            }else{

                                if(rename($data[$name]['tmp_name'], $data[$name]['tmp_name'].'.'.$tmpExt)){
                                    $tmpLen = strlen(\Globals::getConfig()->app_path.\Globals::getConfig()->dir.\Globals::getConfig()->fileupload)+1;
                                    if($Item->Pola[$name]['value']!=''){
                                        unlink(\Globals::getConfig()->app_path.\Globals::getConfig()->dir.\Globals::getConfig()->fileupload.'/'.$Item->Fields[$name]['value']);
                                    }

                                    $Item->Pola[$name]['value'] = substr($data[$name]['tmp_name'],$tmpLen).'.'.$tmpExt;
                                    //var_dump($Item->Pola[$name]['value']);die();
                                }else{
                                    unlink($data[$name]['tmp_name']);
                                    $this->flashMessenger()->setNamespace($cfg['RouteName'].'edit')->addErrorMessage('Błąd! Nie udało się zmienić nazwy pliku '.$field['Name'].'. Spróbuj ponownie.');
                                }
                            }
                        }
                                    }else{
                                       $Item->Pola[$name]['value'] = $data[$name];
                                    //   var_dump($Item->Pola[$name]['value']);
                                    }
                                    
                 }
                 }
                  if($cfg['ListType']=='tree'){//drzewko - dodać PrevId
                        $Item->setPrevId($data['PrevId']);
                    }
                   
                    if($id){                        
                        $Item->Pola[$cfg['OuterField']]['value']=$id;
                    }
                    $user = \Globals::getLoggedIn();
                    if($user)
                    {
                        $Item->setAddingId($user);
                        $Item->setEditingId($user);
                        $Item->setAddingDate(date("Y-m-d H:i:s"));
                        $Item->setEditingDate(date("Y-m-d H:i:s"));
                    }
                        
                  //  die();
                 $itemid = $Item->save();
                 
                 if(is_array($cfg['Seo']))
                {        
                    $seo = $this->stripZnaki($post["seo"]);
                    if(isset($seo) && $seo != "")//jest wpis seo
                    {
                        $seofieldname = strtolower(str_replace(array(" "),array("-"),trim($seo)));
                        //sprawdzenie czy w bazie seo jest już taka nazwa
                         $searchfield=array(
                            "Seo" => $seofieldname,         
                        );                     
                        $seolink = new \Boxy\Model\Seo($searchfield);  
                        if($seolink->Seo!="" && $seolink->PageId!=$itemid)//w bazie jest już identyczny wpis dla innej podstrony -> wygenerować domyślny
                        {
                            $pola = $Item->getFields();
                            $seofieldname = $this->stripZnaki($pola[$cfg['Seo']['Fieldname']]['value']);                  
                            $link = $this->generatelink($seofieldname,1,"");
                            $link = strtolower(str_replace(array(" "),array("-"),trim($link)));                            
                            $seoinsert = new \Boxy\Model\Seo(); 
                            $seoinsert->setSeo($link);
                            $seoinsert->setPageId($itemid);
                            $seoinsert->setModule($cfg['RouteName']);
                            $seoinsert->setRoute($cfg['Seo']['Route']);
                            $seoinsert->setAction($cfg['Seo']['Action']);
                            $seoid = 0;
                            $seosearch=array(
                                "PageId" => $itemid,         
                                "Module" => $cfg['RouteName'],         
                            );  
                            $checkidseo = new \Boxy\Model\Seo($seosearch); 
                            if($checkidseo->Id!="")//jest wpis dla tej strony i modułu->zrobić update
                            {                                
                                $seoinsert->setId($checkidseo->Id);
                            }                              
                            $seoinsert->save();                           
                        }
                        else//wpisu nie ma w bazie->save()
                        {
                            $seolink->setSeo($seofieldname);
                            $seolink->setPageId($itemid);
                            $seolink->setModule($cfg['RouteName']);
                            $seolink->setRoute($cfg['Seo']['Route']);
                            $seolink->setAction($cfg['Seo']['Action']);
                            $searchfield=array(
                                "Module" => $cfg['RouteName'],
                                "PageId" => $itemid,
                            );
                            $seo = new \Boxy\Model\Seo($searchfield); 
                            $seoid = 0;
                            if($seo->Id!="")//jest wpis dla tej strony i modułu->zrobić update
                            {
                                $seoid = $seo->Id;
                                $seolink->setId($seoid);
                            }                              
                            $seolink->save();                           
                        }                        
                    }
                    else 
                    {
                        $pola = $Item->getFields();
                        $seofieldname = $pola[$cfg['Seo']['Fieldname']]['value'];                  
                        $link = $this->generatelink($seofieldname,1,"");
                        $link = strtolower(str_replace(array(" "),array("-"),trim($link)));                            
                        $seoinsert = new \Boxy\Model\Seo(); 
                        $seoinsert->setSeo($link);
                        $seoinsert->setPageId($itemid);
                        $seoinsert->setModule($cfg['RouteName']);
                        $seoinsert->setRoute($cfg['Seo']['Route']);
                        $seoinsert->setAction($cfg['Seo']['Action']);                                                    
                        $seoinsert->save();     
                    }
                }
                
                 $this->flashMessenger()->setNamespace($cfg['RouteName'].'addinfo')->addMessage('Dodawanie zakończone powodzeniem. Możesz dodać następny wpis');
                 if($id!=0)
                 {
                     return $this->redirect()->toRoute($cfg['RouteName'], array('action'=>'add' , 'id'=>$id));
                 }
                 else
                 {
                     return $this->redirect()->toRoute($cfg['RouteName'], array('action'=>'add'));
                 }
                 
            }else{
                 $this->flashMessenger()->setNamespace($cfg['RouteName'].'add')->addErrorMessage('Błąd! Nie wypełniłeś wszystkich wymaganych pól.');
                return $this->redirect()->toRoute($cfg['RouteName'], array('action'=>'add'));
            }
            
            return $this->redirect()->toRoute($cfg['RouteName'], array('action'=>'add'));
   }
   
   public function edytujAction(){
      
            $viewModel = new \Zend\View\Model\ViewModel();
            $viewModel->setTerminal(true); //!!! wyłącza layout !!!

            $striptagsFilter = new \Zend\Filter\StripTags();
            
            $response = $this->getResponse();
            $response->setStatusCode(200);  //!!! zatrzumuje rendering !!!
            
            $cfg = $this->getServiceLocator()->get('ModuleManager')->getModule($this->NazwaModulu)->getConfig();
            $cfg = $cfg['cms_config'];
            
            $id = $striptagsFilter->filter($this->getEvent()->getRouteMatch()->getParam('id'));
            
            $form = new AddForm(null, $this->getServiceLocator());
            $post = array_merge_recursive($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray());            
            $this->flashMessenger()->setNamespace($cfg['RouteName'].'editform')->addMessage($this->getRequest()->getPost());
            
            $form->setData($post);
            if($form->isValid()){
         
            $data = $form->getData();
                $Item = new $cfg['ModelName']($id,$this->getServiceLocator());
                $err = false;
                         
                 foreach($cfg['Pola'] as $name => $field){    
                    if($field['Form']){
                     if($field['FormType']=='file')
                     {//obsługa pliku
                        if($data[$name]['name']!=''){
                            $validatorChain = new \Zend\Validator\ValidatorChain();
                            $tmpExt = explode(".", $data[$name]['name']);
                            if(is_array($tmpExt)){
                                $tmpExt = strtolower($tmpExt[count($tmpExt)-1]);
                            }

                            if(is_array($field['Mime'])){
                                /*$validatorChain->attach(
                                    new \Zend\Validator\File\MimeType($field['Mime'])
                                );*/
                                if(!in_array($data[$name]['type'], $field['Mime'])){
                                    $this->flashMessenger()->setNamespace($cfg['RouteName'].'add')->addErrorMessage('Błąd! Plik '.$field['Name'].' posiada nieodpowiedni typ (mimetype).');
                                    $err =true;
                                }
                            }
                            if(is_array($field['Ext'])){
                                /*$validatorChain->attach(
                                    new \Zend\Validator\File\Extension($field['Mime'])
                                );*/

                                if(!in_array($tmpExt, $field['Ext'])){
                                    $this->flashMessenger()->setNamespace($cfg['RouteName'].'add')->addErrorMessage('Błąd! Plik '.$field['Name'].' posiada nieodpowiednie rozszerzenie.');
                                    $err =true;
                                }
                            }
                            if($field['FileMaxSize']!=null){
                                $validatorChain->attach(
                                    new \Zend\Validator\File\Size($field['FileMaxSize'].'kB')
                                );
                            }
                            if($field['ImgMinSize']!=null){
                                $validatorChain->attach(
                                    new \Zend\Validator\File\ImageSize(array(
                                        'minWidth' => $field['ImgMinSize'],
                                        'minHeight' => $field['ImgMinSize']
                                        )
                                    )
                                );
                            }
                            if($field['ImgMaxSize']!=null){
                                $validatorChain->attach(
                                    new \Zend\Validator\File\ImageSize(array(
                                        'maxWidth' => $field['ImgMaxSize'],
                                        'maxHeight' => $field['ImgMaxSize']
                                        )
                                    )
                                );
                            }

                            if(!$validatorChain->isValid($data[$name]['tmp_name']) || $err){
                                //tutaj wypisanie errorów
                                unlink($data[$name]['tmp_name']);
                                //var_dump($validatorChain->getMessages());
                                foreach($validatorChain->getMessages() as $key => $message){
                                    switch($key){

                                        case 'fileSizeTooBig':
                                            $this->flashMessenger()->setNamespace($cfg['RouteName'].'add')->addErrorMessage('Błąd! Plik '.$field['Name'].' jest zbyt duży. Maksymalny rozmiar pliku to '.$field['FileMaxSize'].'kB.');
                                        break;
                                        case 'fileImageSizeWidthTooBig':
                                            $this->flashMessenger()->setNamespace($cfg['RouteName'].'add')->addErrorMessage('Błąd! Plik '.$field['Name'].' ma za dużą szerokość. Maksymalny wymiar pliku to '.$field['ImgMaxSize'].' x '.$field['ImgMaxSize'].' px');
                                        break;
                                        case 'fileImageSizeHeightTooBig':
                                            $this->flashMessenger()->setNamespace($cfg['RouteName'].'add')->addErrorMessage('Błąd! Plik '.$field['Name'].' ma za dużą wysokość. Maksymalny wymiar pliku to '.$field['ImgMaxSize'].' x '.$field['ImgMaxSize'].' px');
                                        break;
                                        case 'fileImageSizeWidthTooSmall':
                                            $this->flashMessenger()->setNamespace($cfg['RouteName'].'add')->addErrorMessage('Błąd! Plik '.$field['Name'].' ma za małą szerokość. Minimalny wymiar pliku to '.$field['ImgMinSize'].' x '.$field['ImgMinSize'].' px');
                                        break;
                                        case 'fileImageSizeHeightTooSmall':
                                            $this->flashMessenger()->setNamespace($cfg['RouteName'].'add')->addErrorMessage('Błąd! Plik '.$field['Name'].' ma za małą wysokość. Minimalny wymiar pliku to '.$field['ImgMinSize'].' x '.$field['ImgMinSize'].' px');
                                        break;
                                        case 'fileImageSizeNotDetected':
                                            $this->flashMessenger()->setNamespace($cfg['RouteName'].'add')->addErrorMessage('Błąd! Plik '.$field['Name'].' nie jest plikiem graficznym');
                                        break;
                                    }
                                }

                                return $this->redirect()->toRoute($cfg['RouteName'], array('action'=>'edit', 'id'=>$id));
                            }else{

                                if(rename($data[$name]['tmp_name'], $data[$name]['tmp_name'].'.'.$tmpExt)){
                                    $tmpLen = strlen(\Globals::getConfig()->app_path.\Globals::getConfig()->dir.\Globals::getConfig()->fileupload)+1;
                                    if($Item->Pola[$name]['value']!=''){
                                        unlink(\Globals::getConfig()->app_path.\Globals::getConfig()->dir.\Globals::getConfig()->fileupload.'/'.$Item->Fields[$name]['value']);
                                    }

                                    $Item->Pola[$name]['value'] = substr($data[$name]['tmp_name'],$tmpLen).'.'.$tmpExt;
                                    //var_dump($Item->Pola[$name]['value']);die();
                                }else{
                                    unlink($data[$name]['tmp_name']);
                                    $this->flashMessenger()->setNamespace($cfg['RouteName'].'edit')->addErrorMessage('Błąd! Nie udało się zmienić nazwy pliku '.$field['Name'].'. Spróbuj ponownie.');
                                }
                            }
                        }
                    }else{                 

                        if($cfg['OuterField']!=$name ){  
                            $Item->Pola[$name]['value'] = $data[$name];
                        }
                    }
                                 
                                    //var_dump($data[$name]);
                 }
                 }
                 if($cfg['ListType']=='tree'){//drzewko - dodać PrevId
                    $Item->setPrevId($data['PrevId']);
                }
                $user = \Globals::getLoggedIn();
                if($user)
                {                    
                    $Item->setEditingId($user);                    
                    $Item->setEditingDate(date("Y-m-d H:i:s"));
                }
                $Item->save();
                if(is_array($cfg['Seo']))
                {
                    $seo = $this->stripZnaki($post["seo"]);
                    if(isset($seo) && $seo != "")//jest wpis seo
                    {
                        $seofieldname = strtolower(str_replace(array(" "),array("-"),trim($seo)));
                        //sprawdzenie czy w bazie seo jest już taka nazwa
                         $searchfield=array(
                            "Seo" => $seofieldname,         
                        );                            
                        $seolink = new \Boxy\Model\Seo($searchfield);  
                        if($seolink->Seo!="" && $seolink->PageId!=$id)//w bazie jest już identyczny wpis dla innej podstrony -> wygenerować domyślny
                        {
                            $pola = $Item->getFields();
                            $seofieldname = $this->stripZnaki($pola[$cfg['Seo']['Fieldname']]['value']);                  
                            $link = $this->generatelink($seofieldname,1,"");
                            $link = strtolower(str_replace(array(" "),array("-"),trim($link)));                            
                            $seoinsert = new \Boxy\Model\Seo(); 
                            $seoinsert->setSeo($link);
                            $seoinsert->setPageId($id);
                            $seoinsert->setModule($cfg['RouteName']);
                            $seoinsert->setRoute($cfg['Seo']['Route']);
                            $seoinsert->setAction($cfg['Seo']['Action']);
                            $seoid = 0;
                            $seosearch=array(
                                "PageId" => $id,         
                                "Module" => $cfg['RouteName'],         
                            );  
                            $checkidseo = new \Boxy\Model\Seo($seosearch); 
                            if($checkidseo->Id!="")//jest wpis dla tej strony i modułu->zrobić update
                            {                                
                                $seoinsert->setId($checkidseo->Id);
                            }                              
                            $seoinsert->save();                           
                        }
                        else//wpisu nie ma w bazie->save()
                        {
                            $seolink->setSeo($seofieldname);
                            $seolink->setPageId($id);
                            $seolink->setModule($cfg['RouteName']);
                            $seolink->setRoute($cfg['Seo']['Route']);
                            $seolink->setAction($cfg['Seo']['Action']);
                            $searchfield=array(
                                "Module" => $cfg['RouteName'],
                                "PageId" => $id,
                            );
                            $seo = new \Boxy\Model\Seo($searchfield); 
                            $seoid = 0;
                            if($seo->Id!="")//jest wpis dla tej strony i modułu->zrobić update
                            {
                                $seoid = $seo->Id;
                                $seolink->setId($seoid);
                            }                              
                            $seolink->save();                           
                        }                        
                    }                   
                }                
                $this->flashMessenger()->setNamespace($cfg['RouteName'].'editinfo')->addMessage('Edycja zakończona powodzeniem. Możesz edytować następny wpis');
                return $this->redirect()->toRoute($cfg['RouteName'], array('action'=>'edit', 'id'=>$id));
            }else{
                $this->flashMessenger()->setNamespace($cfg['RouteName'].'editinfo')->addErrorMessage('Błąd! Nie wypełniłeś wszystkich wymaganych pól.');
                return $this->redirect()->toRoute($cfg['RouteName'], array('action'=>'edit', 'id'=>$id));
            }
            
            return $this->redirect()->toRoute($cfg['RouteName'], array('action'=>'edit', 'id'=>$id));
   }

   public function editAction(){
        $return = array();
        if($this->getRequest()->isGet()){
            $striptagsFilter = new \Zend\Filter\StripTags();
            $cfg = $this->getServiceLocator()->get('ModuleManager')->getModule($this->NazwaModulu)->getConfig();
            $cfg = $cfg['cms_config'];
            
            $return['config'] = $cfg;
            $id = $striptagsFilter->filter($this->getEvent()->getRouteMatch()->getParam('id'));
            
            $form = new AddForm(null, $this->getServiceLocator());        
            
            if ($this->flashMessenger()->setNamespace($cfg['RouteName'].'edit')->hasErrorMessages()) {
                $return['error'] = implode("<br />", $this->flashMessenger()->setNamespace($cfg['RouteName'].'edit')->getErrorMessages());
            }
            if($this->flashMessenger()->setNamespace($cfg['RouteName'].'editform')->hasMessages()){
            $messages = $this->flashMessenger()->setNamespace($cfg['RouteName'].'editform')->getMessages();            
                if(is_array($messages)){
                    $form->bind($messages[0]);
                }
            }
            if($this->flashMessenger()->setNamespace($cfg['RouteName'].'editinfo')->hasMessages()){
                $return['message'] = implode("<br />", $this->flashMessenger()->setNamespace($cfg['RouteName'].'editinfo')->getMessages());
            }  
              
            $Item = new $cfg['ModelName']($id, $this->getServiceLocator());
            $form->bind($Item);
            
            $addingId = $Item->getAddingId();
            if(is_numeric($addingId) && $addingId>0)
            {
                $sql = "select * from users where id=".$addingId." order by id asc limit 1";
                $user = \Globals::executeDBQuerry($sql);
                if($user->count()>0)
                {
                    $return["adduser"] = $user->current();
                }
            }
            $return['addingDate'] = $Item->getAddingDate();
            
            $editingId = $Item->getEditingId();
            if(is_numeric($editingId) && $editingId>0)
            {
                if($editingId==$addingId)
                {
                    $return["edituser"] = $return["adduser"];
                }
                else
                {
                    $sql = "select * from users where id=".$editingId." order by id asc limit 1";
                    $user = \Globals::executeDBQuerry($sql);
                    if($user->count()>0)
                    {
                        $return["edituser"] = $user->current();
                    }
                }
            }
            $return['editingDate'] = $Item->getEditingDate();            
            /*
             * seo friendly link
             */
         
            if(is_array($cfg['Seo']))
            {
                $searchfield=array(
                    "Module" => $cfg['RouteName'],
                    "PageId" => $id,
                );
                $seo = new \Boxy\Model\Seo($searchfield);
                if($seo->Id!="" && $seo->Seo!="")//jest wpis w bazie seo->przekazać do widoku
                {
                    $return['seolink'] = $seo->Seo;
                } 
                else//brak wpisu w bazie seo->wygenerować wpis domyślny
                {
                    $pola = $Item->getFields();
                    $seofieldname = $pola[$cfg['Seo']['Fieldname']]['value'];                  
                    $seolink = $this->stripZnaki($this->generatelink($seofieldname,1,""));
                   
                    $return['seolink'] = strtolower(str_replace(array(" "),array("-"),trim($seolink)));
                }
            }            
            $return['id'] = $id; 
            $return['form'] = $form;                
            $return['item'] = $Item;
            
        }
        
        return $return;
    }
    public function generatelink( $seofieldname,$level,$oryginallink){
        $searchfield=array(
            "Seo" => $seofieldname,         
        );
        if($oryginallink=="")
        {
            $oryginallink = $seofieldname;
        }
        $seolink = new \Boxy\Model\Seo($searchfield);  
        $seolinkname = "";
        if($seolink->Seo!="")//jest wpis w bazie seo->wygenerować wpis z kolejną cyfrą
        {
            $level++;
            $seofieldname = $oryginallink."-".$level;
            $seolinkname = $this->generatelink($seofieldname, $level, $oryginallink);           
        }
        else 
        {
            $seolinkname = $seofieldname;
        }
        return $seolinkname;
    }
    public function usunplikAction(){
        $viewModel = new \Zend\View\Model\ViewModel();
        $viewModel->setTerminal(true); //!!! wyłącza layout !!!
        
        $response = $this->getResponse();
        $response->setStatusCode(200);
        
        if($this->getRequest()->isXmlHttpRequest()){
            $striptagsFilter = new \Zend\Filter\StripTags();
            $cfg = $this->getServiceLocator()->get('ModuleManager')->getModule($this->NazwaModulu)->getConfig();
            $cfg = $cfg['cms_config'];
            
            $Item = new $cfg['ModelName']($striptagsFilter->filter($this->getRequest()->getPost('delid')), $this->getServiceLocator());
            
            $fieldname = $striptagsFilter->filter($this->getRequest()->getPost('delname'));
            
            if($Item->Pola[$fieldname]['value']!=''){
                if(unlink(\Globals::getConfig()->app_path.\Globals::getConfig()->dir.\Globals::getConfig()->fileupload.'/'.$Item->Pola[$fieldname]['value'])){
                    $Item->Pola[$fieldname]['value']='';
                    $Item->save();
                    $response->setContent(1);
                }else{
                    $response->setContent(0);
                }
            }else{
                $response->setContent(0);
            }
       }
       return $response;
    }
    
    /**
     * zamienia znaki w stringu na alfanumeryczne i zamienia znaki diakrytyczne na odpowiedniki
     * 
     * @param string $string
     * @return string
     */
    public function stripZnaki($string = ''){
        /*$filter = new \Zend\I18n\Filter\Alnum(true);
        $string = $filter->filter($string);
         */
      
        
        //$string = html_entity_decode(preg_replace("/U\+([0-9A-F]{4})/", "&#x\\1;", $string), ENT_NOQUOTES, 'UTF-8');
         
        $arr = array(
		'Ĳ' => 'I', 'Ö' => 'O','Œ' => 'O','Ü' => 'U','ä' => 'a','æ' => 'a',
		'ĳ' => 'i','ö' => 'o','œ' => 'o','ü' => 'u','ß' => 's','ſ' => 's',
		'À' => 'A','Á' => 'A','Â' => 'A','Ã' => 'A','Ä' => 'A','Å' => 'A',
		'Æ' => 'A','Ā' => 'A','Ą' => 'A','Ă' => 'A','Ç' => 'C','Ć' => 'C',
		'Č' => 'C','Ĉ' => 'C','Ċ' => 'C','Ď' => 'D','Đ' => 'D','È' => 'E',
		'É' => 'E','Ê' => 'E','Ë' => 'E','Ē' => 'E','Ę' => 'E','Ě' => 'E',
		'Ĕ' => 'E','Ė' => 'E','Ĝ' => 'G','Ğ' => 'G','Ġ' => 'G','Ģ' => 'G',
		'Ĥ' => 'H','Ħ' => 'H','Ì' => 'I','Í' => 'I','Î' => 'I','Ï' => 'I',
		'Ī' => 'I','Ĩ' => 'I','Ĭ' => 'I','Į' => 'I','İ' => 'I','Ĵ' => 'J',
		'Ķ' => 'K','Ľ' => 'K','Ĺ' => 'K','Ļ' => 'K','Ŀ' => 'K','Ł' => 'L',
		'Ñ' => 'N','Ń' => 'N','Ň' => 'N','Ņ' => 'N','Ŋ' => 'N','Ò' => 'O',
		'Ó' => 'O','Ô' => 'O','Õ' => 'O','Ø' => 'O','Ō' => 'O','Ő' => 'O',
		'Ŏ' => 'O','Ŕ' => 'R','Ř' => 'R','Ŗ' => 'R','Ś' => 'S','Ş' => 'S',
		'Ŝ' => 'S','Ș' => 'S','Š' => 'S','Ť' => 'T','Ţ' => 'T','Ŧ' => 'T',
		'Ț' => 'T','Ù' => 'U','Ú' => 'U','Û' => 'U','Ū' => 'U','Ů' => 'U',
		'Ű' => 'U','Ŭ' => 'U','Ũ' => 'U','Ų' => 'U','Ŵ' => 'W','Ŷ' => 'Y',
		'Ÿ' => 'Y','Ý' => 'Y','Ź' => 'Z','Ż' => 'Z','Ž' => 'Z','à' => 'a',
		'á' => 'a','â' => 'a','ã' => 'a','ā' => 'a','ą' => 'a','ă' => 'a',
		'å' => 'a','ç' => 'c','ć' => 'c','č' => 'c','ĉ' => 'c','ċ' => 'c',
		'ď' => 'd','đ' => 'd','è' => 'e','é' => 'e','ê' => 'e','ë' => 'e',
		'ē' => 'e','ę' => 'e','ě' => 'e','ĕ' => 'e','ė' => 'e','ƒ' => 'f',
		'ĝ' => 'g','ğ' => 'g','ġ' => 'g','ģ' => 'g','ĥ' => 'h','ħ' => 'h',
		'ì' => 'i','í' => 'i','î' => 'i','ï' => 'i','ī' => 'i','ĩ' => 'i',
		'ĭ' => 'i','į' => 'i','ı' => 'i','ĵ' => 'j','ķ' => 'k','ĸ' => 'k',
		'ł' => 'l','ľ' => 'l','ĺ' => 'l','ļ' => 'l','ŀ' => 'l','ñ' => 'n',
		'ń' => 'n','ň' => 'n','ņ' => 'n','ŉ' => 'n','ŋ' => 'n','ò' => 'o',
		'ó' => 'o','ô' => 'o','õ' => 'o','ø' => 'o','ō' => 'o','ő' => 'o',
		'ŏ' => 'o','ŕ' => 'r','ř' => 'r','ŗ' => 'r','ś' => 's','š' => 's',
		'ť' => 't','ù' => 'u','ú' => 'u','û' => 'u','ū' => 'u','ů' => 'u',
		'ű' => 'u','ŭ' => 'u','ũ' => 'u','ų' => 'u','ŵ' => 'w','ÿ' => 'y',
		'ý' => 'y','ŷ' => 'y','ż' => 'z','ź' => 'z','ž' => 'z','Α' => 'A',
		'Ά' => 'A','Ἀ' => 'A','Ἁ' => 'A','Ἂ' => 'A','Ἃ' => 'A','Ἄ' => 'A',
		'Ἅ' => 'A','Ἆ' => 'A','Ἇ' => 'A','ᾈ' => 'A','ᾉ' => 'A','ᾊ' => 'A',
		'ᾋ' => 'A','ᾌ' => 'A','ᾍ' => 'A','ᾎ' => 'A','ᾏ' => 'A','Ᾰ' => 'A',
		'Ᾱ' => 'A','Ὰ' => 'A','ᾼ' => 'A','Β' => 'B','Γ' => 'G','Δ' => 'D',
		'Ε' => 'E','Έ' => 'E','Ἐ' => 'E','Ἑ' => 'E','Ἒ' => 'E','Ἓ' => 'E',
		'Ἔ' => 'E','Ἕ' => 'E','Ὲ' => 'E','Ζ' => 'Z','Η' => 'I','Ή' => 'I',
		'Ἠ' => 'I','Ἡ' => 'I','Ἢ' => 'I','Ἣ' => 'I','Ἤ' => 'I','Ἥ' => 'I',
		'Ἦ' => 'I','Ἧ' => 'I','ᾘ' => 'I','ᾙ' => 'I','ᾚ' => 'I','ᾛ' => 'I',
		'ᾜ' => 'I','ᾝ' => 'I','ᾞ' => 'I','ᾟ' => 'I','Ὴ' => 'I','ῌ' => 'I',
		'Θ' => 'T','Ι' => 'I','Ί' => 'I','Ϊ' => 'I','Ἰ' => 'I','Ἱ' => 'I',
		'Ἲ' => 'I','Ἳ' => 'I','Ἴ' => 'I','Ἵ' => 'I','Ἶ' => 'I','Ἷ' => 'I',
		'Ῐ' => 'I','Ῑ' => 'I','Ὶ' => 'I','Κ' => 'K','Λ' => 'L','Μ' => 'M',
		'Ν' => 'N','Ξ' => 'K','Ο' => 'O','Ό' => 'O','Ὀ' => 'O','Ὁ' => 'O',
		'Ὂ' => 'O','Ὃ' => 'O','Ὄ' => 'O','Ὅ' => 'O','Ὸ' => 'O','Π' => 'P',
		'Ρ' => 'R','Ῥ' => 'R','Σ' => 'S','Τ' => 'T','Υ' => 'Y','Ύ' => 'Y',
		'Ϋ' => 'Y','Ὑ' => 'Y','Ὓ' => 'Y','Ὕ' => 'Y','Ὗ' => 'Y','Ῠ' => 'Y',
		'Ῡ' => 'Y','Ὺ' => 'Y','Φ' => 'F','Χ' => 'X','Ψ' => 'P','Ω' => 'O',
		'Ώ' => 'O','Ὠ' => 'O','Ὡ' => 'O','Ὢ' => 'O','Ὣ' => 'O','Ὤ' => 'O',
		'Ὥ' => 'O','Ὦ' => 'O','Ὧ' => 'O','ᾨ' => 'O','ᾩ' => 'O','ᾪ' => 'O',
		'ᾫ' => 'O','ᾬ' => 'O','ᾭ' => 'O','ᾮ' => 'O','ᾯ' => 'O','Ὼ' => 'O',
		'ῼ' => 'O','α' => 'a','ά' => 'a','ἀ' => 'a','ἁ' => 'a','ἂ' => 'a',
		'ἃ' => 'a','ἄ' => 'a','ἅ' => 'a','ἆ' => 'a','ἇ' => 'a','ᾀ' => 'a',
		'ᾁ' => 'a','ᾂ' => 'a','ᾃ' => 'a','ᾄ' => 'a','ᾅ' => 'a','ᾆ' => 'a',
		'ᾇ' => 'a','ὰ' => 'a','ᾰ' => 'a','ᾱ' => 'a','ᾲ' => 'a','ᾳ' => 'a',
		'ᾴ' => 'a','ᾶ' => 'a','ᾷ' => 'a','β' => 'b','γ' => 'g','δ' => 'd',
		'ε' => 'e','έ' => 'e','ἐ' => 'e','ἑ' => 'e','ἒ' => 'e','ἓ' => 'e',
		'ἔ' => 'e','ἕ' => 'e','ὲ' => 'e','ζ' => 'z','η' => 'i','ή' => 'i',
		'ἠ' => 'i','ἡ' => 'i','ἢ' => 'i','ἣ' => 'i','ἤ' => 'i','ἥ' => 'i',
		'ἦ' => 'i','ἧ' => 'i','ᾐ' => 'i','ᾑ' => 'i','ᾒ' => 'i','ᾓ' => 'i',
		'ᾔ' => 'i','ᾕ' => 'i','ᾖ' => 'i','ᾗ' => 'i','ὴ' => 'i','ῂ' => 'i',
		'ῃ' => 'i','ῄ' => 'i','ῆ' => 'i','ῇ' => 'i','θ' => 't','ι' => 'i',
		'ί' => 'i','ϊ' => 'i','ΐ' => 'i','ἰ' => 'i','ἱ' => 'i','ἲ' => 'i',
		'ἳ' => 'i','ἴ' => 'i','ἵ' => 'i','ἶ' => 'i','ἷ' => 'i','ὶ' => 'i',
		'ῐ' => 'i','ῑ' => 'i','ῒ' => 'i','ῖ' => 'i','ῗ' => 'i','κ' => 'k',
		'λ' => 'l','μ' => 'm','ν' => 'n','ξ' => 'k','ο' => 'o','ό' => 'o',
		'ὀ' => 'o','ὁ' => 'o','ὂ' => 'o','ὃ' => 'o','ὄ' => 'o','ὅ' => 'o',
		'ὸ' => 'o','π' => 'p','ρ' => 'r','ῤ' => 'r','ῥ' => 'r','σ' => 's',
		'ς' => 's','τ' => 't','υ' => 'y','ύ' => 'y','ϋ' => 'y','ΰ' => 'y',
		'ὐ' => 'y','ὑ' => 'y','ὒ' => 'y','ὓ' => 'y','ὔ' => 'y','ὕ' => 'y',
		'ὖ' => 'y','ὗ' => 'y','ὺ' => 'y','ῠ' => 'y','ῡ' => 'y','ῢ' => 'y',
		'ῦ' => 'y','ῧ' => 'y','φ' => 'f','χ' => 'x','ψ' => 'p','ω' => 'o',
		'ώ' => 'o','ὠ' => 'o','ὡ' => 'o','ὢ' => 'o','ὣ' => 'o','ὤ' => 'o',
		'ὥ' => 'o','ὦ' => 'o','ὧ' => 'o','ᾠ' => 'o','ᾡ' => 'o','ᾢ' => 'o',
		'ᾣ' => 'o','ᾤ' => 'o','ᾥ' => 'o','ᾦ' => 'o','ᾧ' => 'o','ὼ' => 'o',
		'ῲ' => 'o','ῳ' => 'o','ῴ' => 'o','ῶ' => 'o','ῷ' => 'o','А' => 'A',
		'Б' => 'B','В' => 'V','Г' => 'G','Д' => 'D','Е' => 'E','Ё' => 'E',
		'Ж' => 'Z','З' => 'Z','И' => 'I','Й' => 'I','К' => 'K','Л' => 'L',
		'М' => 'M','Н' => 'N','О' => 'O','П' => 'P','Р' => 'R','С' => 'S',
		'Т' => 'T','У' => 'U','Ф' => 'F','Х' => 'K','Ц' => 'T','Ч' => 'C',
		'Ш' => 'S','Щ' => 'S','Ы' => 'Y','Э' => 'E','Ю' => 'Y','Я' => 'Y',
		'а' => 'A','б' => 'B','в' => 'V','г' => 'G','д' => 'D','е' => 'E',
		'ё' => 'E','ж' => 'Z','з' => 'Z','и' => 'I','й' => 'I','к' => 'K',
		'л' => 'L','м' => 'M','н' => 'N','о' => 'O','п' => 'P','р' => 'R',
		'с' => 'S','т' => 'T','у' => 'U','ф' => 'F','х' => 'K','ц' => 'T',
		'ч' => 'C','ш' => 'S','щ' => 'S','ы' => 'Y','э' => 'E','ю' => 'Y',
		'я' => 'Y','ð' => 'd','Ð' => 'D','þ' => 't','Þ' => 'T','ა' => 'a',
		'ბ' => 'b','გ' => 'g','დ' => 'd','ე' => 'e','ვ' => 'v','ზ' => 'z',
		'თ' => 't','ი' => 'i','კ' => 'k','ლ' => 'l','მ' => 'm','ნ' => 'n',
		'ო' => 'o','პ' => 'p','ჟ' => 'z','რ' => 'r','ს' => 's','ტ' => 't',
		'უ' => 'u','ფ' => 'p','ქ' => 'k','ღ' => 'g','ყ' => 'q','შ' => 's',
		'ჩ' => 'c','ც' => 't','ძ' => 'd','წ' => 't','ჭ' => 'c','ხ' => 'k',
		'ჯ' => 'j','ჰ' => 'h');
        
     
        return str_replace(array_keys($arr), array_values($arr), $string);
    }
    
}
