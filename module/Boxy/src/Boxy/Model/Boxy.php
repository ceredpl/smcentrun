<?php
namespace Boxy\Model;

class Boxy
{
     /** @var int(11) */
    public $Id;
    /** @var int(11) */
    public $PrevId;    
    /** @var array or null */
    public $Pola = null;
    /** @var string or null */
    public $DBTable = null;
    
    public $AddingDate;
    /** @var int(11) */
    public $AddingId;
    /** @var datetime */
    public $EditingDate;
    /** @var int(11) */
    public $EditingId;
    
    
    
  protected $ServiceLocator = null;
    protected $Config = null;
    
    private $ModuleName = 'Boxy';
    
    public function setServiceLocator(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator) {
        $this->ServiceLocator = $serviceLocator;
    }
    
    public function getServiceLocator() {
        return $this->ServiceLocator;
    }
    
    public function __construct($Id=0, $ServiceLocator=null)
    {
        if($ServiceLocator!=null){
            $this->ServiceLocator = $ServiceLocator;
        }
        
        if($this->ServiceLocator != null){
            $cfg = $this->ServiceLocator->get('ModuleManager')->getModule($this->ModuleName)->getConfig();
            $this->Pola = $cfg['cms_config']['Pola'];
            $this->DBTable = $cfg['cms_config']['Table'];
            $this->Config = $cfg['cms_config'];
            
            
        }
        
        if ($Id) {
             $this->load($Id);
        } else {
            $this->setId(0);
        }
    }
    public function load($Id=0){
        $obj = '';
        if($Id>0){//wczytanie danych z tabeli $this->DBTable
            $tmpsql = 'select * from '.$this->DBTable.' where Id='.$Id.'';
            $obj = \Globals::executeDBQuerry($tmpsql);
        }        
        if($obj->count()>0){//sprawdzenie czy dane są wczytane
            $obj = $obj->current();//przypisanie do zmiennej $obj pierwszego wczytanego wiersza
            if(is_array($this->Pola)){//sprawdzenie czy są zadeklawowane pola
                foreach ($this->Pola as $field => $value) {//pętla po polach
                    if($value['Lang']){//sprawdzenie czy pole jest w wersjach językowych
                        $tmpname = $field.strtoupper(\Globals::getLang());//dopisanie do zadeklarowane nazwy rozszerzenia języka np NazwaPL.
                        $this->Pola[$field]['value'] = $obj->$tmpname;
                    }else{
                        $this->Pola[$field]['value'] = $obj->$field;//przypisanie wartości
                    }
                           
                }
            }            
            if(isset($obj->Id)){
                $this->Id = $obj->Id;//ustawienie zmiennej id jeżeli zistała przekazana w parametrze funkcji
            }           
            if(isset($obj->AddingDate)){
                $this->AddingDate = $obj->AddingDate;
            }
            if(isset($obj->AddingId)){
                $this->AddingId = $obj->AddingId;
            }
            if(isset($obj->EditingDate)){
                $this->EditingDate = $obj->EditingDate;
            }
            if(isset($obj->EditingId)){
                $this->EditingId = $obj->EditingId;
            }
            if($this->Config['ListType']=='tree' && isset($obj->PrevId)){
                $this->PrevId = $obj->PrevId;//ustawienie zmiennej PrevId jeśli ListType jest tree
            }           
        }
    }
    public function save(){
         $db = new \Zend\Db\TableGateway\TableGateway($this->DBTable, \Globals::getDBConnection());
         $lastinsertvalue=0;
         $data = array();
         if(is_array($this->Pola)){
             foreach ($this->Pola as $field => $value) {
                if($value['Lang']){
                    if(!$this->Id){// dodanie nowego wpisu - pola we wszystkich językach ma tę samą wartość
                        foreach(\Globals::getLangs() as $lang){
                                $tmpname = $field.strtoupper($lang);
                                $data[$tmpname] = $value['value'];
                        }
                    }else{
                            $tmpname = $field.strtoupper(\Globals::getLang());
                            $data[$tmpname] = $value['value'];
                    }
                }else{	//var_dump($value['value']);				  
                           $data[$field] = $value['value'];					   
                }			
             }
         }
      
         if($this->Config['ListType']=='tree' && isset($this->PrevId)){
             $data['PrevId'] = $this->PrevId;
         }
         $data['AddingDate'] = $this->AddingDate;
         $data['AddingId'] = $this->AddingId;
         $data['EditingDate'] = $this->EditingDate;
         $data['EditingId'] = $this->EditingId;
         if(!$this->Id){
             $data['Stan'] = "0";   
             $db->insert($data);    
             $lastinsertvalue = $db->getLastInsertValue();
         }else{
             $db->update($data, 'Id = '.(int)  $this->Id);
         }
         
        
        return $lastinsertvalue;    
    }
         public function delete(){
        $db = new \Zend\Db\TableGateway\TableGateway($this->DBTable, \Globals::getDBConnection());
        if($this->Id){           
            $db->delete('Id='.$this->Id);
        }
    }
    public function getId() {
      return $this->Id;
  }

  public function setId($Id) {
      $this->Id = $Id;
  }
  
  public function getPrevId() {
      return $this->PrevId;
  }

  public function setPrevId($PrevId) {
      $this->PrevId = $PrevId;
  }

  public function getAddingDate() {
      return $this->AddingDate;
  }

  public function setAddingDate($AddingDate) {
      $this->AddingDate = $AddingDate;
  }

  public function getAddingId() {
      return $this->AddingId;
  }

  public function setAddingId($AddingId) {
      $this->AddingId = $AddingId;
  }

  public function getEditingDate() {
      return $this->EditingDate;
  }

  public function setEditingDate($EditingDate) {
      $this->EditingDate = $EditingDate;
  }

  public function getEditingId() {
      return $this->EditingId;
  }

  public function setEditingId($EditingId) {
      $this->EditingId = $EditingId;
  }

  public function setFields($Fields) {
      $this->Pola = $Fields;
  }

  public function getFields() {
      return $this->Pola;
  }
   public function getArrayCopy(){
         $data = array();
         
         if(is_array($this->Pola)){
             foreach ($this->Pola as $field => $value) {
                 if($value['FormType']!='password'){ //nie wrzuca hasła do forma edycji                   
                    $data[$field] = $value['value'];
                 }
             }
         }
         $data['Id'] = $this->Id;
         if($this->Config['ListType']=='tree'){
             $data['PrevId'] = $this->PrevId;
         }
         $data['AddingDate'] = $this->AddingDate;
         $data['AddingId'] = $this->AddingId;
         $data['EditingDate'] = $this->EditingDate;
         $data['EditingId'] = $this->EditingId;
        
        
        return $data;
    }
    
    }
