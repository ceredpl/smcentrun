<?php
namespace Boxy\Form;


use Zend\Form\Form;

class Add extends Form
{
    protected $ServiceLocator = null;
    protected $NazwaModulu="Boxy";
    private $Select = array();
  
    
    public function __construct($name = null, $servicelocator = null)
    {
      if($servicelocator!=null){
            $this->ServiceLocator = $servicelocator;
            $cfg = $servicelocator->get('ModuleManager')->getModule($this->NazwaModulu)->getConfig();
            $cfg = $cfg['cms_config'];
            
            parent::__construct($cfg['RouteName']);
            $this->setAttribute('method', 'post');
            
            
            if($cfg['ListType']=='tree'){ //drzewko - dodać pole wyboru rodzica
            $this->prepareTree(0,$cfg);
            $value_options = $this->Select;
            
            $this->add(array(
                'type' => 'Zend\Form\Element\Select',
                'name' => 'PrevId',
                'options' => array(
                    'label' => 'Pozycja nadrzędna *',
                    'allow_remove' => true,
                    'value_options' => $value_options,
                ),
               
            ));
            
        }
        
        
             if(is_array($cfg['Pola'])){
                 $inputFilter = new \Zend\InputFilter\InputFilter();
                 foreach ($cfg['Pola'] as $name => $field) {//pętla po polach configu
                     if($field['Form']){//sprawdzenie czy pole jest w formie, np żeby nie było stanu w formie
                         switch($field['FormType']){//sprawdzenie typu pola
                             case 'input':
                                 $atrybuty = array();
                                    $atrybuty['type'] = 'text';
                                    if($field['Wymagany']){//sprawdzenie czy pole jest wymagane
                                        $atrybuty['required'] = 'required';
                                        $atrybuty['placeholder'] = $field['Placeholder'];
                                    }
                                 $this->add(array(//dodanie do forma
                                    'name' => $name,
                                    'attributes' => $atrybuty,
                                    'options' => array(
                                        'label' => $field['Nazwa'].($field['Wymagany']?' *':''),
                                        'allow_remove' => true,
                                    ),
                                    'filters' => array(
                                        array('name' => 'StripTags'),
                                        array('name' => 'StringTrim'),
                                    ),
                                )); 
                             break;
                             case 'textarea':
                                 $atrybuty = array();
                                    $atrybuty['type'] = 'textarea';
                                    if($field['Wymagany']){//sprawdzenie czy pole jest wymagane
                                        $atrybuty['required'] = 'required';
                                        $atrybuty['placeholder'] = $field['Placeholder'];
                                    }
                                 $this->add(array(//dodanie do forma
                                    'name' => $name,
                                    'attributes' => $atrybuty,
                                    'options' => array(
                                        'label' => $field['Nazwa'].($field['Wymagany']?' *':''),
                                        'allow_remove' => true,
                                        'label_attributes' => array(
                                            'class' => 'wide',
                                        ),
                                    ),
                                    'filters' => array(
                                        array('name' => 'StripTags'),
                                        array('name' => 'StringTrim'),
                                    ),
                                )); 
                             break;
                             case 'wyswig':
                                 $atrybuty = array();
                                    $atrybuty['type'] = 'textarea';
                                    $atrybuty['class'] = 'wyswig';
                                    if($field['Wymagany']){//sprawdzenie czy pole jest wymagane
                                        $atrybuty['required'] = 'required';
                                        $atrybuty['placeholder'] = $field['Placeholder'];
                                    }
                                 $this->add(array(//dodanie do forma
                                    'name' => $name,
                                    'attributes' => $atrybuty,
                                    'options' => array(
                                        'label' => $field['Nazwa'].($field['Wymagany']?' *':''),
                                        'allow_remove' => true,
                                        'label_attributes' => array(
                                            'class' => 'wide',
                                        ),
                                    ),
                                    'filters' => array(
                                        array('name' => 'StripTags'),
                                        array('name' => 'StringTrim'),
                                    ),
                                )); 
                             break;                        
                        case 'checkbox':
                            $this->add(array(
                                'type' => 'Zend\Form\Element\Checkbox',
                                'name' => $name,
                                'options' => array(
                                    'label' => $field['Nazwa'].($field['Wymagany']?' *':''),
                                    'allow_remove' => true,
                                    'use_hidden_element' => true,
                                    'unchecked_value' => '0',
                                    'checked_value' => '1',
                                ),
                                'required' => $field['Wymagany'],
                            ));
                        break;
                        case 'file':
                            /**
                             * @todo obsłużyć pliki                            
                            */
                            $attr = array();
                            if($field['Wymagany']){
                                $attr['required'] = 'required';
                            }
                            $this->add(array(
                                'type' => 'Zend\Form\Element\File',
                                'name' => $name,
                                'attributes' => $attr,
                                'options' => array(
                                    'label' => $field['Nazwa'].' ('.implode(', ', $field['Ext']).(($field['ImgMinSize']!=null && $field['ImgMaxSize']!=null)?'; '.$field['ImgMinSize'].'x'.$field['ImgMinSize'].' - '.$field['ImgMaxSize'].'x'.$field['ImgMaxSize']:'').($field['FileMaxSize']!=null?'; max.'.$field['FileMaxSize'].'kB':'').')'.($field['Wymagany']?' *':''),
                                    'allow_remove' => true,
                                    'label_attributes' => array(
                                        'class' => 'clear-left',
                                    ),
                                ),
                                'required' => $field['Wymagany'],
                            ));
                            
                            
                            $isFilter = true;
                            $fileInput = new \Zend\InputFilter\FileInput($name);
                            $fileInput->setRequired($field['Wymagany']);
                            $fileInput->getFilterChain()->attachByName(
                                'filerenameupload',
                                array(
                                    'target' => \Globals::getConfig()->app_path.\Globals::getConfig()->dir.\Globals::getConfig()->fileupload.'/'.$name,
                                    'randomize' => true,
                                )
                            );
                            $inputFilter->add($fileInput);
                            if($isFilter){
                                $this->setInputFilter($inputFilter);
                            }
                        break;
                        case 'select':
                            $value_options = array();
                            if(is_array($field['Values'])){ //opcje hard-coded
                                $value_options = $field['Values'];
                            }elseif($field['DBTable']!=''){ //opcje pobierane z bazy
                                $sql = 'select '.$field['DBId'].' as Id, '.$field['DBField'].($field['DBLang']?\Globals::getLang():'').' as Field from '.$field['DBTable'].($field['DBWhere']!=''?' where '.$field['DBWhere']:'');
                                
                                $value_options[0] = 'brak';                              
                                $query = \Globals::executeDBQuerry($sql);
                                if(isset($query) && $query->count()>0){
                                    foreach ($query as $value) {
                                        $value_options[$value->Id] = $value->Field;
                                    }
                                }
                            }
                            
                            
                            $this->add(array(
                                'type' => 'Zend\Form\Element\Select',
                                'name' => $name,
                                'options' => array(
                                    'label' => $field['Nazwa'].($field['Wymagany']?' *':''),
                                    'allow_remove' => true,
                                    'value_options' => $value_options,
                                ),
                                'required' => $field['Wymagany'],
                            ));                          
                            
                        break;
                        case 'lat':
                            $atrybuty = array();   
                               if($field['Wymagany']){//sprawdzenie czy pole jest wymagane
                                   $atrybuty['required'] = 'required';
                                   $atrybuty['placeholder'] = $field['Placeholder'];
                               }
                            $this->add(array(//dodanie do forma
                               'name' => $name,
                               'attributes' => $atrybuty,
                               'options' => array(
                                   'label' => $field['Nazwa'].($field['Wymagany']?' *':''),
                                   'allow_remove' => true,                                  
                               ),
                               'filters' => array(
                                   array('name' => 'StripTags'),
                                   array('name' => 'StringTrim'),
                               ),
                           )); 
                        break;
                        case 'lon':
                            $atrybuty = array();   
                               if($field['Wymagany']){//sprawdzenie czy pole jest wymagane
                                   $atrybuty['required'] = 'required';
                                   $atrybuty['placeholder'] = $field['Placeholder'];
                               }
                            $this->add(array(//dodanie do forma
                               'name' => $name,
                               'attributes' => $atrybuty,
                               'options' => array(
                                   'label' => $field['Nazwa'].($field['Wymagany']?' *':''),
                                   'allow_remove' => true,                                  
                               ),
                               'filters' => array(
                                   array('name' => 'StripTags'),
                                   array('name' => 'StringTrim'),
                               ),
                           )); 
                        break;
                        case 'address':
                            $atrybuty = array();   
                               if($field['Wymagany']){//sprawdzenie czy pole jest wymagane
                                   $atrybuty['required'] = 'required';                                   
                               }
                               $atrybuty['class']="address";
                            $this->add(array(//dodanie do forma
                               'name' => $name,
                               'attributes' => $atrybuty,
                               'options' => array(
                                   'label' => $field['Nazwa'].($field['Wymagany']?' *':''),
                                   'allow_remove' => true,                                  
                               ),
                               'filters' => array(
                                   array('name' => 'StripTags'),
                                   array('name' => 'StringTrim'),
                               ),
                           )); 
                        break;
                         case 'multipleselect':
                            $value_options = array();
                            if(is_array($field['Values'])){ //opcje hard-coded
                                $value_options = $field['Values'];
                            }elseif($field['DBTable']!=''){ //opcje pobierane z bazy
                                $sql = 'select '.$field['DBId'].' as Id, '.$field['DBField'].($field['DBLang']?\Globals::getLang():'').' as Field from '.$field['DBTable'].($field['DBWhere']!=''?' where '.$field['DBWhere']:' order by id');                               
                                
                                $query = \Globals::executeDBQuerry($sql);
                          /*
                           * tutaj poprawić przy okazji
                           */
							if(isset($cityId)){
								$tmpsql = 'select language_id from cities_languages where city_id='.$cityId.'';
								$jezyki = \Globals::executeDBQuerry($tmpsql);
							}
							$jezykiArray=array();
							$j=0;
							if(isset($jezyki) && $jezyki->count()>0){
								foreach ($jezyki as $p) {
									$jezykiArray[$j]=$p['language_id'];
									$j++;
								}  
							}			
                               // $value_options[0] = '==Pozycja główna==';
                                if(isset($query) && $query->count()>0){
									$i=0;
                                    foreach ($query as $value) {                                    
                                        $value_options[$i]['value']=$value->id;                                        
                                        $value_options[$i]['label']=$value->field;                                        
                                        $value_options[$i]['selected']=(in_array($value->id,$jezykiArray)?true:false);        
										$i++;										
                                    }
                                }
                            }
                           
							//if($jezyki->count()>0){
						//		     
						//	}					
                             
                            $this->add(array(
                                'type' => 'Zend\Form\Element\Select',
                                'name' => $name,
                                'options' => array(
                                    'label' => $field['Nazwa'].($field['Wymagany']?' *':''),									
                                    'allow_remove' => true,
                                    'value_options' => $value_options,
									'selected' => true,
                                    'value' => '6',								
                                ),
                                'attributes' => array(
                                    'multiple' => 'multiple',
                                   
                                    
                                ),
                                'required' => $field['Wymagany'],
                               
                                
                  
                            ));                       
                            
                        break;
                        case 'date':
                            $attr = array();
                            $d = strtotime("90-12-10");
                            $attr['min'] = date('Y-m-d',$d);
							
                            $attr['max'] = '2099-01-01';
                            $attr['step'] = '1';
                            if($field['Wymagany']){
                                $attr['required'] = 'required';
                            }
                            if($field['DbType']=='datetime'){
                                $attr['class'] = 'datetimepicker';
                            }                            
                            $this->add(array(
                                'type' => ($field['DbType']=='datetime'?'text':'Zend\Form\Element\Date'),
                                'name' => $name,
                                'attributes' => $attr,
                                'options' => array(
                                    'label' => $field['Nazwa'].($field['Wymagany']?' *':''),
                                    'allow_remove' => true,
                                ),
                                'required' => $field['Wymagany'],
                            ));                            
                        break;
                        case 'colorpicker':
                            $attr = array();
                            $attr['type'] = 'text';
                            $attr['class'] = 'colorpicker';
                            if($field['Wymagany']){
                                $attr['required'] = 'required';
                            }
                            
                            $this->add(array(
                                'name' => $name,
                                'attributes' => $attr,
                                'options' => array(
                                    'label' => $field['Nazwa'].($field['Wymagany']?' *':''),
                                    'allow_remove' => true,
                                ),
                                'required' => $field['Wymagany'],
                                'filters' => array(
                                    array('name' => 'StripTags'),
                                    array('name' => 'StringTrim'),
                                ),
                            ));                    
                        break;
                         }
                         
                     }
                     
                 }
                 
             }
            $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Zapisz',
                'id' => 'submitbutton',
            ),
        ));
        }
        
        
    }

  
    private function prepareTree($prev=0, $cfg, $depth=0){
        if($prev==0){
            $this->Select[0] = '==Pozycja główna==';
        }
         
        $sql = 'select t.Id';
        if(is_array($cfg['ListFields'])){
            foreach ($cfg['ListFields'] as $field=>$name){
                if($cfg['Pola'][$field]['DBTable']!=''){//pole zasilane z innej. tabeli
                    $sql .= ', (select '.$cfg['Pola'][$field]['DBField'].($cfg['Pola'][$field]['DBLang']?strtoupper(\Globals::getLang()):'').' from '.$cfg['Pola'][$field]['DBTable'].' where '.$cfg['Pola'][$field]['DBId'].'=t.'.$field.($cfg['Pola'][$field]['Lang']?strtoupper(\Globals::getLang()):'').') as '.$field;
                }else{//po prostu pole
                    $sql .= ', t.'.$field.($cfg['Pola'][$field]['Lang']?strtoupper(\Globals::getLang()):'').' as '.$field;
                }
            }
        }
        $sql .= ' from '.$cfg['Table'].' t where t.PrevId='.$prev.' order by Kolej asc';
        
        $rows = \Globals::executeDBQuerry($sql);
        
        if($rows->count()>0 && $depth+1<$cfg['Glebokosc']){
            foreach ($rows as $row){
               
                if(is_array($cfg['ListFields'])){
                    $tmpStr = '·';
                    for($i=0; $i<$depth; $i++){
                        $tmpStr .= '=·';
                    }
                    foreach ($cfg['ListFields'] as $field=>$name){
                        $tmpStr .= $row->$field.' ';
                    }
                    
                    $this->Select[$row->Id] = $tmpStr;
                    
                    $this->prepareTree($row->Id, $cfg, $depth+1);
                }
            }
        }
        
    }
    
}