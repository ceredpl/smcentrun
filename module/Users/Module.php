<?php
namespace Users;
use Zend\EventManager\StaticEventManager; //!!!

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function init()
 {
 // !!!
   
     $events = StaticEventManager::getInstance();
     $events->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', array($this, 'mvcPreDispatch'), 100);
     
 }
 
 //!!!
 public function mvcPreDispatch($event)
 {
        $routeMatch = $event->getRouteMatch();
        $controllerName = $routeMatch->getParam('controller');//zmienna controller z routingu
        $actionName     = $routeMatch->getParam('action');//zmienna action z routingu
        $log= new  \Zend\Authentication\AuthenticationService();//instancja klasy AuthenticationService
        $url = $event->getRouter()->getBaseUrl();// bazowy adres url aplikacji
        $home=$routeMatch->getMatchedRouteName();// ścieżka domowa
        $sesja=new \Zend\Session\Container();//sesja
       
        /*
         * Sprawdzanie czy zalogowany 
         */
         if(!$log->hasIdentity() && $home!="users"){
            $response = $event->getResponse ();
            $response->setHeaders ( $response->getHeaders ()->addHeaderLine ( 'Location', $url."/users" ));
            $response->setStatusCode ( 302 );
            $response->sendHeaders ();
            
        }else{ 
            /*
             * redirect w przypadku gdy użytkownikjest zalogowany i 
             * chce przejść jeszcze raz do okienka logowania  
             */
            if($log->hasIdentity() && $home=="users"){
                 $response = $event->getResponse ();                    
                $response->setStatusCode ( 302 );
                $response->sendHeaders ();                                
            }
            
        /*
         * Sprawdzanie czy posiada uprawnienia
         */
        $acl=\Globals::getACL();
         if(isset($sesja->rola)) { 
           
            if($acl->isAllowed($sesja->rola,$controllerName.','.$actionName)){
                
               /*
                * Posiada uprawnienia
                */            
            }else{
                /*
                 * Nie posiada uprawnień
                 */
                $kontroler=array();
                $kontroler=explode('\\',$controllerName);
                $ilosc=count($kontroler);
                $error= new \Zend\Mvc\Controller\Plugin\FlashMessenger();
                $error->addErrorMessage("Nie masz uprawnień do oglądania tej strony");
    
                $response = $event->getResponse ();
                $response->setHeaders ( $response->getHeaders ()->addHeaderLine ( 'Location', $url.'/'.strtolower($kontroler[$ilosc-1]).'/error' ));
                $response->setStatusCode ( 302 );
                $response->sendHeaders ();
               

            }   
         }
        }
        

 }
}
