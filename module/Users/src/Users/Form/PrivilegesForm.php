<?php
namespace Users\Form;

use Zend\Form\Form;

class PrivilegesForm extends Form
{
    public function __construct($name = null)
    {
      
        parent::__construct('users');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'login',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Login',
            ),
        ));
        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type'  => 'password',
            ),
            'options' => array(
                'label' => 'Password',
            ),
        ));
   
        
    }
}