<?php

namespace Users\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;
use Zend\Session\SessionManager;
use Users\Form\UsersForm; 
use Zend\Authentication\Adapter\DbTable as AuthAdapter;

class UsersController extends AbstractActionController
{
    public function indexAction()
    {
        $form=new UsersForm();
      
      
        $error="";
       
        if($this->flashMessenger()->hasErrorMessages()){
           $error=implode("<br>",$this->flashMessenger()->getErrorMessages());
          }
     
          return array("form"=>$form, "error"=>$error);
    }
    
    
    public function loginAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $dbAdapter=\globals::getDBConnection();
            $username=$this->params()->fromPost('login');
            $password=$this->params()->fromPost('password');
            
            /*
             * tutaj haszowanie
             */           
            $password=md5($password);
            
            $authAdapter = new AuthAdapter($dbAdapter);//instancja klasy AuthAdapter 
            $authAdapter //ustawienie parametrów tabeli użytkowników
                ->setTableName('users')
                ->setIdentityColumn('username')
                ->setCredentialColumn('password');
            $authAdapter//ustawienie wpisanych przez użytkownika parametrów
                ->setIdentity($username)
                ->setCredential($password);
            $auth= new  \Zend\Authentication\AuthenticationService();
            $auth->authenticate($authAdapter);
            if($auth->hasIdentity()){ //sprawdzenie poprawności danych
                //wczytanie danych zalogowanego użytkownika.
                $querry='Select id_rola, stan from users where username=\''.$username.'\' limit 1'; 
                $user=\globals::executeDBQuerry($querry);
                
                 foreach($user as $i=>$w){
                     $container = new Container();
                     $container->rola = $w->id_rola;
                     if($w->stan==0){//spawdzenie czy użytkownik jest aktywna
                          $this->flashMessenger()->addErrorMessage('Użytkownik nieaktywny');  
                          return $this->redirect()->toRoute('users', array('action'=>'logout'));
                     }
                     
          }
            
          
                return $this->redirect()->toRoute('application', array('action'=>'index'));
                
            }else{
                $this->flashMessenger()->addErrorMessage('Błędny login lub/i hasło');  
                return $this->redirect()->toRoute('users', array('action'=>'index'));
             }
        }else{
           $this->flashMessenger()->addErrorMessage('nie wypełniono wszystkich pól');  
            
        }
                          
       
    
    }
    
    public function logoutAction(){
        $auth= new  \Zend\Authentication\AuthenticationService();
        $auth->clearIdentity();//czyszczenie informacji o zalogowanym użytkowniku
        $container = new Container();
        $container->rola=null;
          
        return $this->redirect()->toRoute('users');
    }
    
public function errorAction(){
      $error=implode("<br>",$this->flashMessenger()->getErrorMessages());
      return array("error"=>$error);
   }   
   
  
   
   
   public function listAction(){
        \globals::getDBConnection();
        $querry='Select id,username,password,imie, nazwisko ,id_rola, stan from users;';
        $uzytkownicy=\globals::executeDBQuerry($querry);
        
       // var_dump($querry);
        
       return array('uzytkownicy'=>$uzytkownicy->toArray());
   }
   
     
   public function changestateAction(){
            $response = $this->getResponse();
            $response->setStatusCode(200);  //!!! zatrzumuje rendering widoku!!!
            $id=$this->getRequest()->getPost('stateid');
            $stan=$this->getRequest()->getPost('stan');
            $stan=($stan+1)%2; //0,1 na zmianę
            $querry='update users set stan=\''.$stan.'\' where id=\''.$id.'\'';
            \globals::executeDBQuerry($querry); 
            $response->setContent(1);
            return $response;       
   }
   
  public function editAction(){
            $response = $this->getResponse();
            $id=$this->getEvent()->getRouteMatch()->getParam('id');
            
            $querry='Select id, username, password, imie, nazwisko, id_rola from users where id=\''.$id.'\';';
            $uzytkownik=\globals::executeDBQuerry($querry);
            
            $querry='Select id, rola from role ;';
            $role=\globals::executeDBQuerry($querry);
            
            return array('uzytkownik'=>$uzytkownik->toArray(),'role'=>$role->toArray());
   }
   public function addAction(){                       
            $querry='Select id, rola from role ;';
            $role=\globals::executeDBQuerry($querry);
            $error=implode("<br />",$this->flashMessenger()->getErrorMessages());
            $message=implode("<br />",$this->flashMessenger()->getMessages());
            
            return array('role'=>$role->toArray(), 'error'=>$error, 'message'=>$message);
   }
   
   public function edytujAction(){
        $response = $this->getResponse();
        $response->setStatusCode(200);  //!!! zatrzumuje rendering !!!
        
        $password=$this->getRequest()->getPost('Haslo');
        $id=$this->getEvent()->getRouteMatch()->getParam('id');
        $login=$this->getRequest()->getPost('Login');
       
        $imie=$this->getRequest()->getPost('Imie');
        $nazwisko=$this->getRequest()->getPost('Nazwisko');
        $rola=$this->getRequest()->getPost('role');       
       
        
        	
        
        if($password!=""){
                $haslo=md5($password);
                $pass='password=\''.$haslo.'\',';
        }
		
		
        $querry='update users set username=\''.$login.'\','.$pass.'  imie=\''.$imie.'\', nazwisko=\''.$nazwisko.'\', id_rola=\''.$rola.'\' where id=\''.$id.'\'';
		
            \globals::executeDBQuerry($querry); 
            
        $this->redirect()->toRoute('users', array('action'=>'edit', 'id'=>$id));
   }
   public function dodajAction(){
        $response = $this->getResponse();
        $response->setStatusCode(200);  //!!! zatrzumuje rendering !!!
        
        $id=$this->getEvent()->getRouteMatch()->getParam('id');
        $login=$this->getRequest()->getPost('Login');
        $haslo=md5($this->getRequest()->getPost('Haslo'));
        $imie=$this->getRequest()->getPost('Imie');
        $nazwisko=$this->getRequest()->getPost('Nazwisko');
        $rola=$this->getRequest()->getPost('role');
        
       
      /*
       * Sprawdzenie czy taki użytkownik istnieje
       */
         $querry='Select * from users where username like "'.$login.'" ;';
         
        $uzytkownik=\globals::executeDBQuerry($querry); 
        
        if($uzytkownik->current()!=null){
             $querry="Taki użytkownik znajduje się już w bazie danych"; 
             $this->flashMessenger()->addErrorMessage($querry);
        }else{
            
            $querry="insert into users(username, password, imie,nazwisko, id_rola, stan) values('".$login."','".$haslo."','".$imie."','".$nazwisko."','".$rola."','0')";
            
            \globals::executeDBQuerry($querry); 
             $this->flashMessenger()->addMessage("Dodano poprawnie");
        }
        
       
            $this->redirect()->toRoute('users', array('action'=>'add'));
        
   }
   public function deleteAction(){
        $response = $this->getResponse();
        $response->setStatusCode(200);  //!!! zatrzumuje rendering !!!
        $id=$this->getRequest()->getPost('id');
        $table=$this->getRequest()->getPost('table');
        $querry='delete from '.$table.' where id=\''.$id.'\'';
        \globals::executeDBQuerry($querry); 
        $response->setContent(1);
        return $response;       
   }
}
