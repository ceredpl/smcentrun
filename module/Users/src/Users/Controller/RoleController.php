<?php

namespace Users\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;
use Zend\Session\SessionManager;
use Users\Form\UsersForm; 
use Zend\Authentication\Adapter\DbTable as AuthAdapter;

class RoleController extends AbstractActionController
{
    
    
    

    
public function errorAction(){
      $error=implode("<br>",$this->flashMessenger()->getErrorMessages());
      return array("error"=>$error);
   }   
   

   
   public function listAction(){
        \globals::getDBConnection();
        $querry='Select id,rola from role;';
        $role=\globals::executeDBQuerry($querry);
        
        //var_dump($querry);
        
       return array('role'=>$role->toArray());
   }
   
  public function editAction(){         
            $id=$this->getEvent()->getRouteMatch()->getParam('id');     
            
            $querry='Select id, rola from role where id=\''.$id.'\';';
            $role=\globals::executeDBQuerry($querry);            
            
            $config = $this->getServiceLocator()->get('Config');
            $konfiguracja=$config['controllers']['invokables'];

            $tablica=array();

            foreach($konfiguracja as $k=>$p){
               $tmp=get_class_methods('\\'.$p);
               foreach($tmp as $l=>$m){

                   $akcja=strstr($m, 'Action',true);
                   if($akcja!=''){
                       if($akcja!='notFound'){
                           if($akcja!='getMethodFrom'){
                               $tmp=substr($p, 0, -10); 
                              
                            $tablica[$tmp][$akcja]=0;
                           }
                       }
                    }
                }
            }      
        $querry='Select id_resource from privileges where id_role=\''.$id.'\';';
        $uprawnienia_roli=\globals::executeDBQuerry($querry);
        $uprawnienia=array();
        foreach($uprawnienia_roli as $k=>$p){
             $querry='Select akcja from resource where id=\''.$p['id_resource'].'\' limit 1;';
             $tmp=\globals::executeDBQuerry($querry);
			 $zmienna=$tmp->current();
             $uprawnienia[$k]=$zmienna['akcja'];
                       
        }

            
        return array('rola'=>$role->toArray(),'uprawnienia'=>$tablica,'uprawnieniaNow'=>$uprawnienia);
   }
   
     public function doprivilegesAction(){
         
        $response = $this->getResponse();
        $response->setStatusCode(200);
            
        $id=$this->getEvent()->getRouteMatch()->getParam('id');
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $odpowiedz=$request->getPost();

            $querry='Select id_resource from privileges where id_role=\''.$id.'\';';
           $uprawnienia_roli=\globals::executeDBQuerry($querry);
           $uprawnienia=array();
           foreach($uprawnienia_roli as $k=>$p){
                $querry='Select id,akcja from resource where id=\''.$p['id_resource'].'\' limit 1;';
                $tmp=\globals::executeDBQuerry($querry);
                $tmp=$tmp->current();
                $uprawnienia[$k]['akcja']=$tmp['akcja'];
                $uprawnienia[$k]['id']=$tmp['id'];
                
           }
      
         $odpowiedzi=array();
       
          foreach($odpowiedz as $i=>$row){
              $odpowiedzi[$i]=$row;
               /*
                * Sprawdzenie czy to nie przycisk edytuj
                */
              if($row!='Edytuj'){
            /*
             * Jeżeli nie ma w uprawnieniach
             */  
               $czyJest=false;   
                $idResource=0;
           foreach($uprawnienia as $k=>$m){
                if($m['akcja']==$row){
                    $czyJest=true;
                    $idResource=$m['id'];
                    break;
                }              
           }
           
              if(!$czyJest){
                $querry='Select id from resource where akcja=\''.str_replace("\\", "\\\\", $row).'\' limit 1;';
                $tmp=\globals::executeDBQuerry($querry);
                $odpowiedz=$tmp->current();
                if($odpowiedz==null){
                    /*
                     *  wstawianie resource jeżeli nie ma w tabeli
                     */
                   $querry='insert into resource (akcja) values (\''.str_replace("\\", "\\\\", $row).'\')';
                   \globals::executeDBQuerry($querry);        
                   /*
                     *  Pobranie Id
                     */
                   $dbAdapter = \globals::$db;
                   $idResource = $dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();                 
                }else{
                    $idResource=$odpowiedz['id'];
                }          
                     $querry='insert into privileges (id_role, id_resource) values ('.$id.', \''.$idResource.'\')';
                    \globals::executeDBQuerry($querry); 
                }
               
          }
            /*
             * Jeżeli jest w uprawnieniach a nie ma w poście
             */
          }
          foreach($uprawnienia as $i=>$row){
               //  var_dump($row);
             if(!in_array($row['akcja'], $odpowiedzi)){
                 $querry='delete from privileges where id_resource='.$row['id'].' and id_role='.$id.'';
                 \globals::executeDBQuerry($querry); 
             }             
          }              
     
    
      $this->redirect()->toRoute('role', array('action'=>'edit', 'id'=>$id));
     }
     }
     
   public function changestateAction(){
            $response = $this->getResponse();
            $response->setStatusCode(200);  //!!! zatrzumuje rendering !!!
            $id=$this->getRequest()->getPost('stateid');
            $stan=$this->getRequest()->getPost('stan');
            $stan=($stan+1)%2; //0,1 na zmianę
            $querry='update users set stan=\''.$stan.'\' where id=\''.$id.'\'';
            \globals::executeDBQuerry($querry); 
            $response->setContent(1);
            return $response;       
   }
   
  
   public function addAction(){                       
           
            $error=implode("<br />",$this->flashMessenger()->getErrorMessages());
            $message=implode("<br />",$this->flashMessenger()->getMessages());
            
            return array( 'error'=>$error, 'message'=>$message);
   }
   public function dodajAction(){
        $response = $this->getResponse();
        $response->setStatusCode(200);  //!!! zatrzumuje rendering !!!
        
        
        $nazwa=$this->getRequest()->getPost('Nazwa');              
       
      /*
       * Sprawdzenie czy taki użytkownik istnieje
       */
         $querry='Select * from role where rola like "'.$nazwa.'" ;';
         
        $rola=\globals::executeDBQuerry($querry); 
        
        if($rola->current()!=null){
             $querry="Taka rola już istnieje"; 
             $this->flashMessenger()->addErrorMessage($querry);
        }else{
            
            $querry="insert into role(rola) values('".$nazwa."')";
            
            \globals::executeDBQuerry($querry); 
             $this->flashMessenger()->addMessage("Dodano poprawnie");
        }      
            $this->redirect()->toRoute('role', array('action'=>'add'));
        
   }
   public function deleteAction(){
        $response = $this->getResponse();
        $response->setStatusCode(200);  //!!! zatrzumuje rendering !!!
        $id=$this->getRequest()->getPost('id');
        $table=$this->getRequest()->getPost('table');
        $querry='delete from '.$table.' where id=\''.$id.'\'';
        \globals::executeDBQuerry($querry); 
        $response->setContent(1);
        return $response;       
   }
}
