<?php
return array(
    /*
     * Deklaracja kontrolerów
     */
    'controllers' => array(
        'invokables' => array(
            'Kontakty\Controller\Kontakty' => 'Kontakty\Controller\KontaktyController',   
            'Kontakty\Controller\Gallery' => 'Kontakty\Controller\GalleryController',   
        ),
    ),
	
    /*
     * Deklaracja tras w module
     */
    'router' => array(
        'routes' => array(            
            'kontakty' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/kontakty[/:action][/:id][/page/:page][/pageid/:pageid]',//domyślny routing                 
                    'constraints' => array(//ograniczenia
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',                       
                        'page'     => '[0-9]+',                       
                        'pageid'     => '[0-9]+',                       
                    ),
                    'defaults' => array(//akcja domyślna
                        'controller' => 'Kontakty\Controller\Kontakty',
                        'action' => 'index',
                        'id' => 0,           
                        'page' => 0,                      
                        'pageid' => 0,                      
                    ),
                ),
            ),
            'kontaktygallery' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/kontakty/gallery[/:action][/:id][/picture/:picid]',
                    'constraints' => array(
                        'picid'     => '[0-9]+', /* id pliku fotki */
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',                       
                    ),
                    'defaults' => array(
                        'picid'         => 0,
                        'controller' => 'Kontakty\Controller\Gallery',
                        'action'     => 'gallery',
                        'id'         => 0,                       
                    ),
                ),
            ),
        ),
    ),

	
    'view_manager' => array(
        'template_path_stack' => array(
            'kontakty' => __DIR__ . '/../view',
        ),
    ),
    
    /*
     * Custom config
     */
    
'cms_config' => array(
    'Table' => 'kontakty',  //nazwa tabeli w bazie
    'ListType'=>'tree', // typ listy: list lub tree      
    'Warunek'=>null,//warunek do tabeli
    'OrderBy'=>'Id asc',//orderby dla tabeli w akcji list
    'RouteName' => 'kontakty',//nazwa trasy
    'ModelName' => '\Kontakty\Model\Kontakty',  //Ścieżka modelu.     
    'Glebokosc' => 3, // głębokość preparowanego drzewa wpisów
    'OuterField' => '',//pole wykorzystywane do wyświetlenia pól z modułu nadrzędnego
    'Options' => null,//tutaj może znajdować się tablica z routingiem do modułów podrzędnych     
    'ModuleName' => 'Kontakty',   /* nazwa modułu (z polskimi znakami) */
    'Rowperpage' => 10,
    'ListFields' => array(          /* lista kolumn w drzewie wczytywanych z bazy */           
          'Nazwa' => 'Nazwa',       //nazwa pola w konfigu = > nazwa wyświetlana     
          'Stan' => 'Stan',            
        ),
    'Seo' => null,
    'Filter' => array(
        'searchfield' => array(//pola po których ma przeszukiwać
            'Nazwa'
        ),
        'sortfields' => array(          
           'Nazwa' => 'Nazwa',//Pole w configu -> frontend            
        ),         
    ),
    'Gallery' => null,
     //deklaracja pól dostępnych w ramach formularza.
    'Pola' => array(//pola w module mapowane na pola w bazie 
        'Nazwa' => array(
            'Nazwa'=>'Nazwa',//nazwa wyświetlana we frontendzie
            'FormType' => 'input',//typ forma
            'DbType' => 'varchar(255)',//typ w bazie
            'Visible' => true,//czy widoczny w formie na stronie z listą
            'Values' => null, //opcje dostępne do wyboru
            'Link'=>true, // sprawdzanie czy jest linkiem do edycji
            'LiveChange'=>null,//czy zmienna jest przełączana ajaxem (zmiana stanu)
            'Lang'=>false,//sprawdzenie czy pole jest w języku
            'Form'=>true,// czy widoczny podczas edycji
            'Wymagany'=>true,
            'Placeholder'=>'Nazwa',
            'Mime' => null,
            'Ext' => null,
            'ImgMinSize' => null,
            'ImgMaxSize' => null,
            'FileMaxSize' => null,
            'DBTable' => null,            // zewnętrzna tabela zasilająca pole
            'DBId' => null,               // id zewnętrznej tabeli 
            'DBField' => null,            // nazwa pola zewnętrznej tabeli 
            'DBLang' => false,           // określa czy pole DBField występuje w werjach jęz. - bool 
            'DBWhere' => '',            // warunek zapytania o wartości (np. 'State=1' - string                           
        ),  
        'Telefon' => array(
            'Nazwa'=>'Telefon',//nazwa wyświetlana we frontendzie
            'FormType' => 'input',//typ forma
            'DbType' => 'varchar(255)',//typ w bazie
            'Visible' => false,//czy widoczny w formie na stronie z listą
            'Values' => null, //opcje dostępne do wyboru
            'Link'=>true, // sprawdzanie czy jest linkiem do edycji
            'LiveChange'=>null,//czy zmienna jest przełączana ajaxem (zmiana stanu)
            'Lang'=>false,//sprawdzenie czy pole jest w języku
            'Form'=>true,// czy widoczny podczas edycji
            'Wymagany'=>true,
            'Placeholder'=>'Nazwa',
            'Mime' => null,
            'Ext' => null,
            'ImgMinSize' => null,
            'ImgMaxSize' => null,
            'FileMaxSize' => null,
            'DBTable' => null,            // zewnętrzna tabela zasilająca pole
            'DBId' => null,               // id zewnętrznej tabeli 
            'DBField' => null,            // nazwa pola zewnętrznej tabeli 
            'DBLang' => false,           // określa czy pole DBField występuje w werjach jęz. - bool 
            'DBWhere' => '',            // warunek zapytania o wartości (np. 'State=1' - string                           
        ),          
        'Stan' => array(
            'Nazwa'=>'Stan',//nazwa wyświetlana we frontendzie
            'FormType' => 'select',//typ forma
            'DbType' => 'int(1)',//typ w bazie
            'Visible' => true,//czy widoczny w formie
            'Values' => array(
                  0 => 'Nieaktywny',
                  1 => 'Aktywny',
              ),
            'Link'=>false,
            'LiveChange'=>true,
            'Lang'=>null,
            'Form'=>null,
            'Wymagany'=>false,
            'Placeholder'=>null,
            'Ext' => null,
            'ImgMinSize' => null,
            'ImgMaxSize' => null,
            'FileMaxSize' => null,
            'DBTable' => null,            /* zewnętrzna tabela zasilająca pole */
            'DBId' => null,               /* id zewnętrznej tabeli */
            'DBField' => null,            /* nazwa pola zewnętrznej tabeli */
            'DBLang' => false,           /* określa czy pole DBField występuje w werjach jęz. - bool */
            'DBWhere' => null,            /* warunek zapytania o wartości (np. 'State=1' - string */   
        ),         
    ),
 ),
);