<?php
namespace Kontakty\Model;

class Seo
{
     /** @var int(11) */
    public $Id;
    /** @var int(11) */
    public $Route;    
    /** @var array or null */
    public $Action;
    /** @var string or null */
    public $Module;
    
    public $PageId;
    /** @var int(11) */
    public $Seo;
    
    public $tableGateway;
   
    
    
    
  protected $ServiceLocator = null;
 
    public function setServiceLocator(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator) {
        $this->ServiceLocator = $serviceLocator;
    }
    
    public function getServiceLocator() {
        return $this->ServiceLocator;
    }
    
    public function __construct($Id=0, $ServiceLocator=null)
    {
        if($ServiceLocator!=null){
            $this->ServiceLocator = $ServiceLocator;
        }           
        $this->tableGateway = new \Zend\Db\TableGateway\TableGateway("seo", \Globals::getDBConnection());
        if ($Id) {
             $this->load($Id);
        } else {
            $this->setId(0);
        }
    }
    public function load($searchfield){
        $newsearchfield = array();
        foreach ($searchfield as $key => $value) {
            if($key=="Seo")
            {
                $newsearchfield[$key.strtoupper(\Globals::getLang())]=$value;
            }
            else
            {
                $newsearchfield[$key]=$value;
            }
        }  
        $rowset = $this->tableGateway->select($newsearchfield);
         
        $row = $rowset->current();
        if($row)
        {
            $se = 'Seo'.strtoupper(\Globals::getLang());
            $this->Id = $row->Id;
            $this->Route = $row->Route;
            $this->Action = $row->Action;
            $this->Module = $row->Module;
            $this->PageId = $row->PageId;
            $this->Seo = $row->$se;
        }
        
        return $row;
    }
    
    public function save(){           
        $data = $this->getArrayCopy(); 
        
        if(!$this->Id){            
            $this->tableGateway->insert($data);             
        }else{
            $this->tableGateway->update($data, 'Id = '.(int)  $this->Id);
        }
    }
    public function delete(){
        $this->tableGateway = new \Zend\Db\TableGateway\TableGateway("seo", \Globals::getDBConnection());
        if($this->PageId){           
            $this->tableGateway->delete('PageId='.$this->PageId,'Module='.$this->Module);
        }
    }
 public function getId() {
      return $this->Id;
  }

  public function setId($Id) {
      $this->Id = $Id;
  }
  
  public function getRoute() {
      return $this->Route;
  }

  public function setRoute($Route) {
      $this->Route = $Route;
  }

  public function getModule() {
      return $this->Module;
  }

  public function setModule($Module) {
      $this->Module = $Module;
  }

  public function getPageId() {
      return $this->PageId;
  }

  public function setPageId($PageId) {
      $this->PageId = $PageId;
  }

  public function getSeo() {
      return $this->Seo;
  }

  public function setSeo($Seo) {
      $this->Seo = $Seo;
  }

  public function setAction($Action) {
      $this->Action = $Action;
  }

  public function getAction() {
      return $this->Action;
  }

   public function getArrayCopy(){
         $data = array();         
         $data['Id'] = $this->Id;        
         $data['Route'] = $this->Route;
         $data['Action'] = $this->Action;
         $data['Module'] = $this->Module;
         $data['PageId'] = $this->PageId;
         $data['Seo'.\Globals::getLang()] = $this->Seo;
        
        
        return $data;
    }
    
}
