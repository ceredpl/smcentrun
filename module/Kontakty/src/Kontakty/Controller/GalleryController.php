<?php


namespace Kontakty\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Kontakty\Model\Kontakty;



class GalleryController extends AbstractActionController{
        
    private $PaginationSlideWidth = 5;
    private $RowsPerPage = 20;
    
    private $ModuleName = 'Kontakty';
    
    /**
     * @todo delete this
     */
    public function indexAction(){
        
    }
    
   /**
     * Renders list of photos
     * 
     * @return view
     */
    public function galleryAction(){
        $striptagsFilter = new \Zend\Filter\StripTags();
        $cfg = $this->getServiceLocator()->get('ModuleManager')->getModule($this->ModuleName)->getConfig();
        $cfg = $cfg['cms_config'];
        $return = array();
        
        $id = $striptagsFilter->filter($this->getEvent()->getRouteMatch()->getParam('id'));
        $return['id'] = $id;
        
        if ($this->flashMessenger()->setNamespace($cfg['RouteName'].'upload')->hasErrorMessages()) {
                $return['error'] = implode("<br />", $this->flashMessenger()->setNamespace($cfg['RouteName'].'upload')->getErrorMessages());
            }
        if($this->flashMessenger()->setNamespace($cfg['RouteName'].'upload')->hasMessages()){
                $return['message'] = implode("<br />", $this->flashMessenger()->setNamespace($cfg['RouteName'].'upload')->getMessages());
            }
            
        if($this->getRequest()->isGet()){
            $breadcrumbs = array();
            
            $breadcrumbs[$cfg['ModuleName']] = $this->url()->fromRoute($cfg['RouteName'], array('action'=>'list'));
            $breadcrumbs['Edycja'] = $this->url()->fromRoute($cfg['RouteName'], array('action'=>'edit', 'id'=>$id));
            $breadcrumbs['Galeria'] = $this->url()->fromRoute($cfg['Gallery']['RouteName'], array('action'=>'gallery', 'id'=>$id));
            
            
          
            
           
           $return['config'] = $cfg;
           
           
           $query = 'select * from '.$cfg['Gallery']['DBTable'].' where '.$cfg['Gallery']['DBId'].'='.$id.' order by Ord asc';
           $items = \Globals::executeDBQuerry($query);
           $return['items'] = $items;
           
        }elseif($this->getRequest()->isXmlHttpRequest()){
            $response = $this->getResponse();
            $response->setStatusCode(200);  //!!! zatrzumuje rendering !!!
            
            if($this->getRequest()->getPost('changevalue')==1){
                $id = $striptagsFilter->filter($this->getRequest()->getPost('id'));
                $state = $striptagsFilter->filter($this->getRequest()->getPost('state'));
                
                $db = new \Zend\Db\TableGateway\TableGateway($cfg['Gallery']['DBTable'], \Globals::getDBConnection());
                $tmpArr = array();
                $tmpArr['State'] = $state;
                $db->update($tmpArr, 'Id='.$id);
                $response->setContent(1);
            }else if($this->getRequest()->getPost('delete')==1){
                $id = $striptagsFilter->filter($this->getRequest()->getPost('id'));
                
                $picToDelete = \Globals::executeDBQuerry('select File, Thumb from '.$cfg['Gallery']['DBTable'].' where Id='.$id);
                if($picToDelete->count()>0){
                    $row = $picToDelete->current();
                    if($row->File!='') unlink(\Globals::getConfig()->app_path.\Globals::getConfig()->pictureupload.'/'.$row->File);
                    if($row->Thumb!='') unlink(\Globals::getConfig()->app_path.\Globals::getConfig()->thumbupload.'/'.$row->Thumb);
                }
                
                $db = new \Zend\Db\TableGateway\TableGateway($cfg['Gallery']['DBTable'], \Globals::getDBConnection());
                $db->delete('Id='.$id);
                $response->setContent(1);
            }else{
                $response->setContent(0);
            }
            return $response;
        }
        
        return $return;
    }
    
   
    
    /**
     * sorts pictures
     * 
     * @return 200
     */
    public function sortAction(){
        $viewModel = new \Zend\View\Model\ViewModel();
        $viewModel->setTerminal(true); //!!! wyłącza layout !!!
        
        $response = $this->getResponse();
        $response->setStatusCode(200);  //!!! zatrzumuje rendering !!!
        
        $cfg = $this->getServiceLocator()->get('ModuleManager')->getModule($this->ModuleName)->getConfig();
        $cfg = $cfg['cms_config'];
        
        $striptagsFilter = new \Zend\Filter\StripTags();
        $id = $striptagsFilter->filter($this->getEvent()->getRouteMatch()->getParam('id')); //id wiersza w bd
        if($this->getRequest()->isXmlHttpRequest()){
            $db = new \Zend\Db\TableGateway\TableGateway($cfg['Gallery']['DBTable'], \Globals::getDBConnection());
            
            $order = $this->getRequest()->getPost('order');
            if(is_array($order)){
                foreach ($order as $key => $value) {
                    $itemId = explode("_", $value);
                    $tmpArr['Ord'] = $key;
                    $db->update($tmpArr, $cfg['Gallery']['DBId'].'='.$id.' and Id='.$itemId[1]);
                }
            }
        }
        
        
        return $response;
    }


    /**
     * uploads files via uploadify
     * 
     * @return 200
     */
    public function uploadAction(){
        $viewModel = new \Zend\View\Model\ViewModel();
        $viewModel->setTerminal(true); //!!! wyłącza layout !!!
        
        $response = $this->getResponse();
        $response->setStatusCode(200);  //!!! zatrzumuje rendering !!!
        
        $striptagsFilter = new \Zend\Filter\StripTags();
        
        $cfg = $this->getServiceLocator()->get('ModuleManager')->getModule($this->ModuleName)->getConfig();
        $cfg = $cfg['cms_config'];
        
        $data = array_merge_recursive($this->getRequest()->getFiles()->toArray(), $this->getRequest()->getPost()->toArray());
        
        
        if(is_array($data)){
//            $tmpExtArr = str_replace("*.", "", $this->getRequest()->getPost()->fileext);
//            $tmpExtArr = explode(";", $tmpExtArr);
            
             $tmpExtArr = $cfg['Gallery']['Extensions'];
             
            $tmpExt = '';
            
            if(isset($data['Filedata']) && $data['Filedata']!=''){
                
                $tmpExt = explode(".", $data['Filedata']['name']);
                if(is_array($tmpExt)){
                    $tmpExt = strtolower($tmpExt[count($tmpExt)-1]);
                }
            }
            
            if(is_array($tmpExtArr)){
                
                if(isset($data['PictureId']) && $data['PictureId']>0){ //zmiana foty
                        $db = new \Zend\Db\TableGateway\TableGateway($cfg['Gallery']['DBTable'], \Globals::getDBConnection());
                        
                        $arr = array();
                        $arr['Description'.(\Globals::getConfig()->langs!=''?\Globals::getLang():'')] = $data['Description'];
                        //var_dump($arr);die();
                        $db->update($arr, 'Id='.$data['PictureId']);

                        $picRow = \Globals::executeDBQuerry('select '.$cfg['Gallery']['DBId'].' as Id from '.$cfg['Gallery']['DBTable'].' where Id='.$data['PictureId']);
                        if($picRow->count()>0){
                            $pic = $picRow->current();
                            return $this->redirect()->toRoute($cfg['Gallery']['RouteName'], array('action'=>'gallery', 'id'=>$pic->Id));
                        }
                    }
                    
                elseif(in_array($tmpExt, $tmpExtArr) && $data['Filedata']!=''){
                    $tmpName = sha1(microtime()).'.'.$tmpExt;
                    $imgSize = getimagesize($data['Filedata']['tmp_name']);
                    
                    if(move_uploaded_file($data['Filedata']['tmp_name'], \Globals::getConfig()->app_path.\Globals::getConfig()->dir.\Globals::getConfig()->pictureupload.'/'.$tmpName)){
                        
                        if(is_array($cfg['Gallery'])){
                            $db = new \Zend\Db\TableGateway\TableGateway($cfg['Gallery']['DBTable'], \Globals::getDBConnection());
                            
                            if($cfg['Gallery']['Watermark']!=''){
                                $this->doWatermark(\Globals::getConfig()->app_path.\Globals::getConfig()->dir.\Globals::getConfig()->pictureupload.'/'.$tmpName, $cfg['Gallery']['Watermark']);
                            }
                            
                            $thumbName = $this->prepareThumbnail(\Globals::getConfig()->app_path.\Globals::getConfig()->dir.\Globals::getConfig()->pictureupload.'/'.$tmpName, $cfg['Gallery']['ThumbMaxWidth'], $cfg['Gallery']['ThumbMaxHeight'], $tmpName);
                            if($thumbName){
                                $arr['Thumb'] = $thumbName;
                            }
                            $arr['Width'] = $imgSize[0];
                            $arr['Height'] = $imgSize[1];
                            $arr['File'] = $tmpName;
                            $arr['Name'] = $data['Filedata']['name'];
                            
                            //upload nowej                                
                                $id = $striptagsFilter->filter($this->getEvent()->getRouteMatch()->getParam('id')); //id wiersza w bd
//                                $id = \Globals::executeDBQuerry('select  COUNT(*) from '.$cfg['Gallery']['DBTable']);
//                                $id=$id->current();
//                                $id=$id['COUNT(*)'];                              
                                $arr['AddingDate'] = date("Y-m-d H:i:s");
                                $arr['State'] = 1;
                                $arr[$cfg['Gallery']['DBId']] = $id;
                                $arr['Ord'] = 0;
                                $db->insert($arr);
                            
                        }
                        $this->flashMessenger()->setNamespace($cfg['RouteName'].'upload')->addMessage('Zdjęcia zostały poprawnie wgrane na serwer');
                        $response->setContent('<div class="zdjecie-wynik"> Zdjęcie: '.$arr['Name'].' zostało poprawnie wgrane na serwer');
                       // return $this->redirect()->toRoute($cfg['Gallery']['RouteName'], array('action'=>'gallery', 'id'=>$pic->Id));
                    }else{
                        unlink($data['Filedata']['tmp_name']);
                        $this->flashMessenger()->setNamespace($cfg['RouteName'].'upload')->addErrorMessage('Wystąpił błąd podczas wgrywania pliku');
                        $response->setContent('<div class="zdjecie-wynik"> Wystąpił błąd podczas wgrywania pliku '.$arr['Name'].' na serwer');
                    }
                }else{
                    
                    
                    $this->flashMessenger()->setNamespace($cfg['RouteName'].'upload')->addErrorMessage('Błędne rozrzerzenie pliku: '.$arr["Name"]);
                    $response->setContent('<div class="zdjecie-wynik"> Błędne rozrzerzenie pliku: '.$arr['Name']);
                }
            }
        }else{
             $response->setContent(4);
        }
        
        return $response;
        
    }
    
    public function descriptionAction(){
        $viewModel = new \Zend\View\Model\ViewModel();
        $viewModel->setTerminal(true); //!!! wyłącza layout !!!
        
        $response = $this->getResponse();
        $response->setStatusCode(200);  //!!! zatrzumuje rendering !!!
        
        $striptagsFilter = new \Zend\Filter\StripTags();
        
        $cfg = $this->getServiceLocator()->get('ModuleManager')->getModule($this->ModuleName)->getConfig();
        $cfg = $cfg['cms_config'];
        
        if($this->getRequest()->isXmlHttpRequest()){
            $id = $striptagsFilter->filter($this->getRequest()->getPost()->id);
            if($id>0 && is_numeric($id)){
                $picRow = \Globals::executeDBQuerry('select Description'.(\Globals::getConfig()->langs!=''?\Globals::getLang():'').' as Description from '.$cfg['Gallery']['DBTable'].' where Id='.$id);
                if($picRow->count()>0){
                    $response->setContent($picRow->current()->Description);
                }
            }
        }
        
        return $response;
    }
    
    /**
     * replaces file with one combined with given watermark
     * 
     * @param string $file
     * @param string $watermark
     * @return boolean
     */
    private function doWatermark($file, $watermark){
        $success = false;
        $watermark_file = \Globals::getConfig()->app_path.$watermark;
        $watermarkSize = getimagesize($watermark_file);
        $imgSize = getimagesize($file);
                
        if($imgSize['mime']=='image/jpeg'){
            $newWatermarkSize = $this->changeSize($watermarkSize[0],$watermarkSize[1],$imgSize[0],$imgSize[1]);
            
            $watermark =  imagecreatetruecolor($newWatermarkSize[0],$newWatermarkSize[1]);
            imagealphablending($watermark, false);
            imagesavealpha($watermark, true);  

            $source = imagecreatefrompng($watermark_file);
            imagealphablending($source, true);

            imagecopyresampled($watermark, $source, 0, 0, 0, 0, $newWatermarkSize[0], $newWatermarkSize[1], $watermarkSize[0], $watermarkSize[1]);

            $thumb = imagecreatetruecolor($imgSize[0],$imgSize[1]);
		
            imagecopyresampled($thumb, imagecreatefromjpeg($file), 0, 0, 0, 0, $imgSize[0], $imgSize[1], $imgSize[0], $imgSize[1]);   
            imagealphablending($thumb, true);
            
            imagecopy($thumb, $watermark, ceil((imagesx($thumb) - imagesx($watermark))/2), ceil((imagesy($thumb) - imagesy($watermark))/2), 0, 0, imagesx($watermark), imagesy($watermark));
            
            imagejpeg($thumb, $file, 100);
            if(imagejpeg($thumb, $file, 100)){
                $success = true;
            }
        }
       
        return $success;
    }
    
    
    /**
     * prepares thumbnail of given file
     * 
     * @param string $file
     * @param int $maxWidth
     * @param int $maxHeight
     */
    private function prepareThumbnail($file, $maxWidth, $maxHeight, $thumbName){
        $imgSize = getimagesize($file);
        $newSize = $this->changeSize($imgSize[0], $imgSize[1], $maxWidth, $maxHeight);
        $w = $newSize[0];
        $h = $newSize[1];
        $thumbPath = \Globals::getConfig()->app_path.\Globals::getConfig()->dir.\Globals::getConfig()->thumbupload.'/';
        
        switch($imgSize['mime']){
            case 'image/jpeg':
                $thumb = imagecreatetruecolor($w,$h);
                imagecopyresampled($thumb, imagecreatefromjpeg($file), 0, 0, 0, 0, $w, $h, $imgSize[0], $imgSize[1]);   
                if(imagejpeg($thumb, $thumbPath.$thumbName, 100)){
                    return $thumbName;
                }else{
                    return false;
                }
            break;
            case 'image/gif':
                $thumb = imagecreatetruecolor($w,$h);
                $backgroundColor = imagecolorallocate($thumb, 255, 255, 255);
                imagefill($thumb, 0, 0, $backgroundColor);
                imagecolortransparent($thumb, $backgroundColor );
                imagecopymerge($thumb,imagecreatefromgif($file), 0, 0, 0, 0, $w, $h, 100);
                if(imagegif($thumb, $thumbPath.$thumbName)){
                    return $thumbName;
                }else{
                    return false;
                }
            break;
            case 'image/png':
                $thumb = imagecreatetruecolor($w, $h);
                imagealphablending($thumb, false);
                imagesavealpha($thumb, true);
                $source = imagecreatefrompng($file);
                imagealphablending($source, true);
                imagecopyresampled($thumb, $source, 0, 0, 0, 0, $w, $h, $imgSize[0], $imgSize[1]);
                if (imagepng($thumb,$thumbPath.$thumbName)){
                    return $thumbName;
                }else{
                    return false;
                }
            break;
        }
        return false;
    }
    
    
    /**
     * converts width and height to max constraint 
     * 
     * @param int $w
     * @param int $h
     * @param int $maxWidth
     * @param int $maxHeight
     * @return array
     */
    private function changeSize($w, $h, $maxWidth, $maxHeight){
            if ($w > $h)
            {               if ($w > $maxWidth)
                    {
                            $skalax = ($maxWidth*100)/$w;
                            $skalax = round($skalax)/100;
                            $w = $maxWidth;
                            $h = $h * $skalax;

                            if ($h > $maxHeight)
                            {
                                    $skalay = ($maxHeight*100)/$h;
                                    $skalay = round($skalay)/100;
                                    $h = $maxHeight;
                                    $w = $w * $skalay;
                            }

                    }

            }
            else
            {
                    if ($h > $maxHeight)
                    {
                            $skalay = ($maxHeight*100)/$h;
                            $skalay = round($skalay)/100;
                            $h = $maxHeight;
                            $w = $w * $skalay;

                            if ($w > $maxWidth)
                            {
                                    $skalax = ($maxWidth*100)/$w;
                                    $skalax = round($skalax)/100;
                                    $w = $maxWidth;
                                    $h = $h * $skalax;
                            }
                    }
            }

            return array($w,$h);
    }

    
}
?>