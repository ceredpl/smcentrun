<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
          
        return new ViewModel();
    }
    public function langAction()
    {
        $viewModel = new \Zend\View\Model\ViewModel();
        $viewModel->setTerminal(true); //!!! wyłącza layout !!!
        
        $lang = $this->getEvent()->getRouteMatch()->getParam('id');//pobranie identyfikatora języka
        if(in_array($lang, \Globals::getLangs())){//sprawdzenie czy język dopuszczalny
            $session = new \Zend\Session\Container();//utworznie sesji
            $session->lang = $lang;//dodanie do sesji języka
        }
        if($_SERVER['HTTP_REFERER']!=''){//przekierowanie
            return $this->redirect()->toUrl($_SERVER['HTTP_REFERER']);
        }else{
            return $this->redirect()->toRoute('home');
        }        
    }
    public function newsletterAction()
    {
        return new ViewModel();
    }
}
