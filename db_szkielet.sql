-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 17 Lut 2015, 11:27
-- Wersja serwera: 5.6.20
-- Wersja PHP: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `db_szkielet`
--

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `privileges` (
`id` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `id_resource` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=177 ;

--
-- Zrzut danych tabeli `privileges`
--

INSERT INTO `privileges` (`id`, `id_role`, `id_resource`) VALUES
(1, 1, 1),
(7, 1, 5),
(12, 2, 5),
(15, 2, 8),
(16, 2, 9),
(17, 2, 10),
(18, 2, 11),
(19, 2, 12),
(20, 2, 13),
(22, 2, 15),
(24, 2, 38),
(25, 2, 40),
(50, 2, 43),
(52, 2, 1),
(53, 2, 2),
(54, 2, 4),
(81, 2, 37),
(82, 2, 60),
(83, 2, 50),
(84, 2, 51),
(85, 2, 53),
(87, 2, 55),
(88, 2, 56),
(89, 2, 61),
(90, 2, 62),
(91, 2, 63),
(92, 2, 52),
(98, 2, 44),
(99, 2, 45),
(100, 2, 49),
(101, 2, 46),
(102, 2, 47),
(103, 2, 48),
(104, 2, 69),
(105, 2, 70),
(106, 2, 71),
(107, 2, 72),
(108, 2, 73),
(109, 2, 74),
(125, 2, 90),
(126, 2, 91),
(127, 2, 92),
(128, 2, 93),
(129, 2, 94),
(130, 2, 95),
(131, 2, 96),
(132, 2, 97),
(133, 2, 98),
(134, 2, 99),
(135, 2, 100),
(136, 2, 101),
(137, 2, 102),
(138, 2, 103),
(139, 2, 104),
(140, 2, 105),
(141, 2, 106),
(142, 2, 107),
(143, 2, 108),
(144, 2, 109),
(145, 2, 110),
(146, 2, 111),
(147, 2, 112),
(148, 2, 113),
(149, 2, 114),
(150, 2, 115),
(151, 2, 116),
(152, 2, 117),
(153, 2, 118),
(154, 2, 119),
(155, 2, 120),
(156, 2, 121),
(157, 2, 122),
(158, 2, 123),
(159, 2, 124),
(160, 2, 125),
(161, 2, 126),
(162, 2, 127),
(163, 2, 128),
(164, 2, 129),
(165, 2, 130),
(166, 2, 131),
(167, 2, 132),
(168, 2, 133),
(169, 2, 134),
(170, 2, 135),
(171, 2, 136),
(172, 2, 137),
(173, 2, 138),
(174, 2, 139),
(175, 2, 140),
(176, 2, 141);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `resource`
--

CREATE TABLE IF NOT EXISTS `resource` (
`id` int(11) NOT NULL,
  `akcja` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=142 ;

--
-- Zrzut danych tabeli `resource`
--

INSERT INTO `resource` (`id`, `akcja`) VALUES
(1, 'Users\\Controller\\Users,index'),
(2, 'Users\\Controller\\Users,login'),
(3, 'Users\\Controller\\Users,indexUser'),
(4, 'Users\\Controller\\Users,logout'),
(5, 'Users\\Controller\\Users,error'),
(6, 'Users\\Controller\\Users,privileges'),
(7, 'Users\\Controller\\Users,doprivileges'),
(8, 'Users\\Controller\\Users,list'),
(9, 'Users\\Controller\\Users,changestate'),
(10, 'Users\\Controller\\Users,edit'),
(11, 'Users\\Controller\\Users,edytuj'),
(12, 'Users\\Controller\\Role,list'),
(13, 'Users\\Controller\\Role,edit'),
(14, 'Users\\Controller\\Role,edytuj'),
(15, 'Users\\Controller\\Role,doprivileges'),
(37, 'Application\\Controller\\Index,index'),
(38, 'Users\\Controller\\Role,error'),
(40, 'Users\\Controller\\Role,index'),
(43, 'Users\\Controller\\Role,changestate'),
(44, 'Users\\Controller\\Users,add'),
(45, 'Users\\Controller\\Users,dodaj'),
(46, 'Users\\Controller\\Role,add'),
(47, 'Users\\Controller\\Role,dodaj'),
(48, 'Users\\Controller\\Role,delete'),
(49, 'Users\\Controller\\Users,delete'),
(50, 'Subpage\\Controller\\Subpage,list'),
(51, 'Subpage\\Controller\\Subpage,delete'),
(52, 'Subpage\\Controller\\Subpage,index'),
(53, 'Subpage\\Controller\\Subpage,changevalue'),
(54, 'Subpage\\Controller\\Subpage,changeKolej'),
(55, 'Subpage\\Controller\\Subpage,add'),
(56, 'Subpage\\Controller\\Subpage,dodaj'),
(60, 'Application\\Controller\\Index,lang'),
(61, 'Subpage\\Controller\\Subpage,edytuj'),
(62, 'Subpage\\Controller\\Subpage,edit'),
(63, 'Subpage\\Controller\\Subpage,usunplik'),
(69, 'Subpage\\Controller\\Subpage,changeTurn'),
(70, 'Subpage\\Controller\\Gallery,index'),
(71, 'Subpage\\Controller\\Gallery,gallery'),
(72, 'Subpage\\Controller\\Gallery,sort'),
(73, 'Subpage\\Controller\\Gallery,upload'),
(74, 'Subpage\\Controller\\Gallery,description'),
(75, 'Newsletteremail\\Controller\\Newsletteremail,import'),
(76, 'Newsletteremail\\Controller\\Newsletteremail,index'),
(77, 'Newsletteremail\\Controller\\Newsletteremail,list'),
(78, 'Newsletteremail\\Controller\\Newsletteremail,sort'),
(79, 'Newsletteremail\\Controller\\Newsletteremail,filter'),
(80, 'Newsletteremail\\Controller\\Newsletteremail,clearfilter'),
(81, 'Newsletteremail\\Controller\\Newsletteremail,addchild'),
(82, 'Newsletteremail\\Controller\\Newsletteremail,inline'),
(83, 'Newsletteremail\\Controller\\Newsletteremail,add'),
(84, 'Newsletteremail\\Controller\\Newsletteremail,processadd'),
(85, 'Newsletteremail\\Controller\\Newsletteremail,edit'),
(86, 'Newsletteremail\\Controller\\Newsletteremail,processedit'),
(87, 'Newsletteremail\\Controller\\Newsletteremail,delete'),
(88, 'Newsletteremail\\Controller\\Newsletteremail,deletefile'),
(89, 'Newsletteremail\\Controller\\Newsletteremail,deletesubfile'),
(90, 'Newslettergroup\\Controller\\Newslettergroup,list'),
(91, 'Newslettergroup\\Controller\\Newslettergroup,delete'),
(92, 'Newslettergroup\\Controller\\Newslettergroup,changevalue'),
(93, 'Newslettergroup\\Controller\\Newslettergroup,changeTurn'),
(94, 'Newslettergroup\\Controller\\Newslettergroup,add'),
(95, 'Newslettergroup\\Controller\\Newslettergroup,dodaj'),
(96, 'Newslettergroup\\Controller\\Newslettergroup,edytuj'),
(97, 'Newslettergroup\\Controller\\Newslettergroup,edit'),
(98, 'Newslettergroup\\Controller\\Newslettergroup,usunplik'),
(99, 'Newslettergroup\\Controller\\Newslettergroup,index'),
(100, 'Application\\Controller\\Index,newsletter'),
(101, 'Newslettermail\\Controller\\Newslettermail,list'),
(102, 'Newslettermail\\Controller\\Newslettermail,delete'),
(103, 'Newslettermail\\Controller\\Newslettermail,changevalue'),
(104, 'Newslettermail\\Controller\\Newslettermail,changeTurn'),
(105, 'Newslettermail\\Controller\\Newslettermail,add'),
(106, 'Newslettermail\\Controller\\Newslettermail,dodaj'),
(107, 'Newslettermail\\Controller\\Newslettermail,edytuj'),
(108, 'Newslettermail\\Controller\\Newslettermail,edit'),
(109, 'Newslettermail\\Controller\\Newslettermail,usunplik'),
(110, 'Newslettermail\\Controller\\Newslettermail,index'),
(111, 'Newslettermessage\\Controller\\Newslettermessage,list'),
(112, 'Newslettermessage\\Controller\\Newslettermessage,delete'),
(113, 'Newslettermessage\\Controller\\Newslettermessage,changevalue'),
(114, 'Newslettermessage\\Controller\\Newslettermessage,changeTurn'),
(115, 'Newslettermessage\\Controller\\Newslettermessage,add'),
(116, 'Newslettermessage\\Controller\\Newslettermessage,dodaj'),
(117, 'Newslettermessage\\Controller\\Newslettermessage,edytuj'),
(118, 'Newslettermessage\\Controller\\Newslettermessage,edit'),
(119, 'Newslettermessage\\Controller\\Newslettermessage,usunplik'),
(120, 'Newslettermessage\\Controller\\Newslettermessage,index'),
(121, 'Newslettermessage\\Controller\\Newslettermessage,send'),
(122, 'Newslettermessagefile\\Controller\\Newslettermessagefile,list'),
(123, 'Newslettermessagefile\\Controller\\Newslettermessagefile,delete'),
(124, 'Newslettermessagefile\\Controller\\Newslettermessagefile,changevalue'),
(125, 'Newslettermessagefile\\Controller\\Newslettermessagefile,changeTurn'),
(126, 'Newslettermessagefile\\Controller\\Newslettermessagefile,add'),
(127, 'Newslettermessagefile\\Controller\\Newslettermessagefile,dodaj'),
(128, 'Newslettermessagefile\\Controller\\Newslettermessagefile,edytuj'),
(129, 'Newslettermessagefile\\Controller\\Newslettermessagefile,edit'),
(130, 'Newslettermessagefile\\Controller\\Newslettermessagefile,usunplik'),
(131, 'Newslettermessagefile\\Controller\\Newslettermessagefile,index'),
(132, 'Settings\\Controller\\Settings,list'),
(133, 'Settings\\Controller\\Settings,delete'),
(134, 'Settings\\Controller\\Settings,changevalue'),
(135, 'Settings\\Controller\\Settings,changeTurn'),
(136, 'Settings\\Controller\\Settings,add'),
(137, 'Settings\\Controller\\Settings,dodaj'),
(138, 'Settings\\Controller\\Settings,edytuj'),
(139, 'Settings\\Controller\\Settings,edit'),
(140, 'Settings\\Controller\\Settings,usunplik'),
(141, 'Settings\\Controller\\Settings,index');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `role`
--

CREATE TABLE IF NOT EXISTS `role` (
`id` int(11) NOT NULL,
  `rola` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Zrzut danych tabeli `role`
--

INSERT INTO `role` (`id`, `rola`) VALUES
(1, 'guest'),
(2, 'admin');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `seo`
--

CREATE TABLE IF NOT EXISTS `seo` (
`Id` int(11) NOT NULL,
  `Route` varchar(255) NOT NULL,
  `Action` varchar(255) NOT NULL,
  `Module` varchar(255) NOT NULL,
  `PageId` int(11) NOT NULL,
  `Seo` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;



--
-- Struktura tabeli dla tabeli `subpage`
--

CREATE TABLE IF NOT EXISTS `kontakty` (
`Id` int(11) NOT NULL,
  `PrevId` int(11) NOT NULL,
  `Nazwa` varchar(255) NOT NULL,
  `Telefon` varchar(255) NOT NULL,  
  `AddingDate` datetime NOT NULL,
  `EditingDate` datetime NOT NULL,
  `AddingId` int(11) NOT NULL,
  `EditingId` int(11) NOT NULL,
  `Stan` int(1) NOT NULL,
  `Kolej` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `boxy` (
`Id` int(11) NOT NULL,
  `PrevId` int(11) NOT NULL,
  `Nazwa` varchar(255) NOT NULL,
  `Opis` longtext NULL,  
  `Ikona` varchar(255) NULL,  
  `IkonaHover` varchar(255) NULL,  
  `AddingDate` datetime NOT NULL,
  `EditingDate` datetime NOT NULL,
  `AddingId` int(11) NOT NULL,
  `EditingId` int(11) NOT NULL,
  `Stan` int(1) NOT NULL,
  `Kolej` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `news` (
`Id` int(11) NOT NULL,
  `PrevId` int(11) NOT NULL,
  `Nazwa` varchar(255) NOT NULL,
  `Opis` longtext NULL,  
  `IsMainPage` int(1) NULL,   
  `AddingDate` datetime NOT NULL,
  `EditingDate` datetime NOT NULL,
  `AddingId` int(11) NOT NULL,
  `EditingId` int(11) NOT NULL,
  `Stan` int(1) NOT NULL,
  `Kolej` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Zrzut danych tabeli `subpage`
--


--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(32) COLLATE utf8_bin NOT NULL,
  `imie` varchar(150) COLLATE utf8_bin NOT NULL,
  `nazwisko` varchar(255) COLLATE utf8_bin NOT NULL,
  `id_rola` int(11) NOT NULL,
  `stan` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `imie`, `nazwisko`, `id_rola`, `stan`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Cezary', 'Redes', 2, 1),
(2, 'guest', 'guest', 'janusz', 'kowlaski', 1, 0);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `privileges`
--
ALTER TABLE `privileges`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resource`
--
ALTER TABLE `resource`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seo`
--
ALTER TABLE `seo`
 ADD PRIMARY KEY (`Id`);

ALTER TABLE `kontakty`
 ADD PRIMARY KEY (`Id`);

ALTER TABLE `news`
 ADD PRIMARY KEY (`Id`);

ALTER TABLE `boxy`
 ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

ALTER TABLE `privileges`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=177;
--
-- AUTO_INCREMENT dla tabeli `resource`
--
ALTER TABLE `resource`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=142;
--
-- AUTO_INCREMENT dla tabeli `role`
--
ALTER TABLE `role`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `seo`
--
ALTER TABLE `seo`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;

ALTER TABLE `kontakty`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

ALTER TABLE `news`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

ALTER TABLE `boxy`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
