<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\EventManager\StaticEventManager;
use Zend\ModuleManager\ModuleManager;
use Zend\Session\Container;

class Module
{
    /*
    public function onBootstrap(MvcEvent $e)
    {
        $translator = $e->getApplication()->getServiceManager()->get('translator');
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }*/
    public function onBootstrap($e)
    {
        $e->getApplication()->getEventManager()->getSharedManager()->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($e) {
            $controller      = $e->getTarget();
            $controllerClass = get_class($controller);
            $moduleNamespace = substr($controllerClass, 0, strpos($controllerClass, '\\'));
            $config          = $e->getApplication()->getServiceManager()->get('config');

            $routeMatch = $e->getRouteMatch();            
          
          
            
           
            //$actionName = strtolower($routeMatch->getParam('action', 'not-found')); // get the action name
          //  var_dump($moduleNamespace);die();
            /*
            if($moduleNamespace=="Badacz" && $actionName=="mlodybadacz")
            {
                $controller->layout('badacz/layout');
                
            }
            if (isset($config['module_layouts'][$moduleNamespace][$actionName])) {
                $controller->layout($config['module_layouts'][$moduleNamespace][$actionName]);
            }elseif(isset($config['module_layouts'][$moduleNamespace]['default'])) {
                $controller->layout($config['module_layouts'][$moduleNamespace]['default']);
            }
*/
           
            
        }, 100);
    }
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
     /**
     * Init function
     *
     * @return void
     */
    public function init(ModuleManager $mm){
        // Attach Event to EventManager
        $events = StaticEventManager::getInstance();
        $events->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', array($this, 'mvcPreDispatch'), 100);
        
        /**
         * jeśli odwiedzona już podstrona grupy - nie wyświetlać layoutu powitalnego
         */
        /*$mm->getEventManager()->getSharedManager()->attach(__NAMESPACE__, 'dispatch', function($e) {
            if(isset($_COOKIE['group_visited'])){
                $e->getTarget()->layout('group/layout');
            }
            $session = new \Zend\Session\Container('langs');
            if(isset($session->motor) && $session->motor==1){
                $e->getTarget()->layout('motor/layout');
            }
            
        });*/
    }
    
    /**
     * MVC preDispatch Event for all actions.
     *
     * @param $event
     * @return mixed
     */
    public function mvcPreDispatch($event){        
       }
}
