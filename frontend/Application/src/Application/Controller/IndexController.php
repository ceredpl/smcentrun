<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Session\Container;


class IndexController extends AbstractActionController
{

    /**
     * Renders main page or redirects to login form (for not logged yet)
     * 
     * @return array
     */
    public function indexAction()
    {
        $return = array();
        $layout = $this->layout();
        $layout->setVariables(array(
            'active' => 'home',
        ));
        
        $phonesSQL = "Select Nazwa, Telefon from kontakty where stan=1 order by kolej asc";
        $phones = \Globals::executeDBQuerry($phonesSQL);
        if(isset($phones) && $phones->count()>0)
        {
            $return['phones'] = $phones;
        }
        
        $boxesSQL = "Select Id, Nazwa, Ikona, IsContact, MailContact from boxy where stan=1 order by kolej asc";
        $boxes = \Globals::executeDBQuerry($boxesSQL);
        if(isset($boxes) && $boxes->count()>0)
        {
            $return['boxes'] = $boxes;
        }
        
        
        
        $newsSQL = "Select Id, Nazwa, Short  from news where stan=1 and IsMainpage=1 order by Id asc limit 5";
        $news = \Globals::executeDBQuerry($newsSQL);
        if(isset($news) && $news->count()>0)
        {
            $return['news'] = $news;
        }

       return $return;
    }
    
    public function seolinksAction()
    {
        $seolink = $this->getEvent()->getRouteMatch()->getParam('parametry');
        $seolinkarray = \Globals::loadSeolinks();
        $linkarray;
        $viewHelperManager = $this->getServiceLocator()->get('ViewHelperManager');
        $layouthelper = $viewHelperManager->get('layoutHelper'); 
        foreach ($seolinkarray as $key => $seo) {
            if($seo['Seo']==$seolink)
            {
                $linkarray = $seo;
                break;
            }
        }        
        if(is_array($linkarray) && count($linkarray)>0)
        {
             $uri = $this->url()->fromRoute($linkarray['Route'],array('action'=>$linkarray['Action'],'id'=>$linkarray['PageId'], 'title'=>$layouthelper->stripZnaki($seolink)));
             //$uri = '/pzr/public/news/kategoria/News,4';
        }
        else 
        {
            $uri = \Globals::getConfig()->siteurl;
        }
        
        $request = new \Zend\Http\Request();
        $request->setUri($uri);

        $router = $this->getServiceLocator()->get('Router');
        $routeMatch = $router->match($request);

        $viewModel = new \Zend\View\Model\ViewModel();

        
        if($routeMatch !== null ) {
            $controller = $routeMatch->getParam('controller');
            $params = $routeMatch->getParams();

            $viewModel = $this->forward()->dispatch($controller, $params);
        }else{
            /**
             * @todo tutaj jakiś redirect do strony głównej, 404 czy coś
             */
        }
        
        return $viewModel;
    }

    public function langAction()
    {
        $viewModel = new \Zend\View\Model\ViewModel();
        $viewModel->setTerminal(true); //!!! wyłącza layout !!!
        
        $lang = $this->getEvent()->getRouteMatch()->getParam('id');
        if(in_array($lang, \Globals::getLangs())){
            $session = new \Zend\Session\Container();
            $session->lang = $lang;
        }
        if($_SERVER['HTTP_REFERER']!=''){
            return $this->redirect()->toUrl($_SERVER['HTTP_REFERER']);
        }else{
            return $this->redirect()->toRoute('home');
        }
        
    }
    /**
     * signs in to newsletter and returns message / error
     */
    public function newsletterAction(){
        $viewModel = new \Zend\View\Model\ViewModel();
        $viewModel->setTerminal(true); //!!! wyłącza layout !!!
        
        $response = $this->getResponse();
        $response->setStatusCode(200);  //!!! zatrzumuje rendering !!!
        
        $return = array();
        $return['success'] = false;
        if($this->getRequest()->isXmlHttpRequest()){
            $post = array();
            
            $post['Email']=$this->getRequest()->getPost('Email');
            if(trim($post['Email'])!=''){
                $emailValidator = new \Zend\Validator\Regex(array('pattern' => '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/'));
                if(!$emailValidator->isValid($post['Email'])){
                    $return['error'] = 'Błąd! Niepoprawny adres e-mail';
                }else{
                    $emailcheckSQL = 'select count(*) as count from newsletter_emails where Email=\''.addslashes($post['Email']).'\'';
                    $emailcheck = \Globals::executeDBQuerry($emailcheckSQL);
                    if($emailcheck->count()>0){
                        $emailcheck = $emailcheck->current();
                        if($emailcheck->count>0){
                            $return['error'] = 'Błąd! Podany adres jest już zapisany do newslettera.';
                        }else{
                            $newsletterGateway = new \Zend\Db\TableGateway\TableGateway('newsletter_emails', \Globals::getDBConnection());
                            $data = array(
                                'Email' => strip_tags($post['Email']),
                                'AddDate' => date('Y-m-d H:i:s'),
                                'State' => 1,
                            );
                            $newsletterGateway->insert($data);
                            
                            $return['message'] = 'Zapisałeś się do newslettera.';
                            $return['success'] = true;
                        }
                    }
                }
            }else{
                $return['error'] = 'Błąd! Nie podałeś adresu e-mail';
            }
        }
       
        $response->setContent(json_encode($return));
        
        return $response;
    }
    
}
