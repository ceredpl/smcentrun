<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class ErrorController extends AbstractActionController
{
    public function errorAction()
    {
        $return = array();
        
        /*if(!\Globals::getLoggedIn()){
            $this->layout()->setTemplate('user/layout');
        }*/
        
        if($this->flashMessenger()->hasMessages()) {
            $return['message'] = implode("<br />", $this->flashMessenger()->getMessages());
        }
        
        return $return;
    }
}
