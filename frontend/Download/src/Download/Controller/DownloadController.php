<?php

namespace Download\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use User\Model\User as User;
use Zend\Http\Headers;
use Zend\Http\Response\Stream;



class DownloadController extends AbstractActionController
{
    
    /**
     * Renders main page or redirects to login form (for not logged yet)
     * 
     * @return array
     */
    public function downloadAction()
    {
        
        $return = array();
        
        $type = $this->getEvent()->getRouteMatch()->getParam('type');
        $id = $this->getEvent()->getRouteMatch()->getParam('id');
        $ext = $this->getEvent()->getRouteMatch()->getParam('ext');
        
        $translate = $this->getServiceLocator()->get('viewhelpermanager')->get('translate');
        
        $fileSQL = '';
        switch($type){
            case 'form':
                if(is_numeric($id) && $id>0 && $ext!=""){                   
                   if($ext=="pdf")
                   {
                       $fileSQL = 'select Id, Nazwa'.mb_strtoupper(\Globals::getLang()).' as Nazwa,FilePDF as File from  formularze where Id='.$id.' limit 1';
                   }
                   elseif($ext=="doc")
                   {
                       $fileSQL = 'select Id, Nazwa'.mb_strtoupper(\Globals::getLang()).' as Nazwa,FileDOC as File from  formularze where Id='.$id.' limit 1';
                   }    
                }else{
                    $return['error'] = $translate('Błąd! Wybrany plik nie istnieje');
                }
            break;
            case 'page':
                if(is_numeric($id) && $id>0){
                    $fileSQL = 'select Id, Nazwa'.mb_strtoupper(\Globals::getLang()).' as Nazwa,File from  subpagefile where Id='.$id.' limit 1';   
                }else{
                    $return['error'] = $translate('Błąd! Wybrany plik nie istnieje');
                }
            break;
            case 'zasob':
                if(is_numeric($id) && $id>0){
                    $fileSQL = 'select Id, Nazwa'.mb_strtoupper(\Globals::getLang()).' as Nazwa,File from  zasobfile where Id='.$id.' limit 1';   
                }else{
                    $return['error'] = $translate('Błąd! Wybrany plik nie istnieje');
                }
            break;
            case 'dysputy':
                if(is_numeric($id) && $id>0){
                    $fileSQL = 'select Id, Nazwa'.mb_strtoupper(\Globals::getLang()).' as Nazwa,File from  dysputyfile where Id='.$id.' limit 1';   
                }else{
                    $return['error'] = $translate('Błąd! Wybrany plik nie istnieje');
                }
            break;
            case 'seminaria':
                if(is_numeric($id) && $id>0){
                    $fileSQL = 'select Id, Nazwa'.mb_strtoupper(\Globals::getLang()).' as Nazwa,File from  seminariafile where Id='.$id.' limit 1';   
                }else{
                    $return['error'] = $translate('Błąd! Wybrany plik nie istnieje');
                }
            break;
            case 'audio':
                if(is_numeric($id) && $id>0){
                    $fileSQL = 'select Id, Nazwa'.mb_strtoupper(\Globals::getLang()).' as Nazwa,File from  wskazowkifile where Id='.$id.' limit 1';   
                }else{
                    $return['error'] = $translate('Błąd! Wybrany plik nie istnieje');
                }
            break;
            default:
                $return['error'] = $translate('Błąd! Nie masz uprawnień do oglądania tej strony');
            break;
        }
        
        $fileQuery = \Globals::executeDBQuerry($fileSQL);                           
        if($fileQuery->count()>0){
            $file = $fileQuery->current();
            if($file->Id==$id && $file->Nazwa!=''){
                $viewModel = new \Zend\View\Model\ViewModel();
                $viewModel->setTerminal(true); //!!! wyłącza layout !!!
                $response = new \Zend\Http\Response\Stream();
                $myHelper = $this->getServiceLocator()->get('viewhelpermanager')->get('LayoutHelper');
                //$result = $myHelper->StripZnaki($file->Nazwa);
                
                
                $finfo = finfo_open(FILEINFO_MIME_TYPE); 
                $filename = \Globals::getConfig()->app_path.\Globals::getConfig()->dir.\Globals::getConfig()->fileupload.'/'.$file->File;
                $ext = pathinfo($filename, PATHINFO_EXTENSION);
                $mimetype = finfo_file($finfo, $filename);
                $result = $file->Nazwa.'.'.$ext;
                
                $response = new Stream();
                $response->setStream(fopen($filename, 'r'));
                $response->setStatusCode(200);
                $response->setStreamName(basename($filename));
                
                 $headers = new Headers();
                 $headers->addHeaders(array(
	            'Content-Disposition' => 'attachment; filename="' .$result.'"',
	            'Content-Type' => 'application/octet-stream',
	            'Content-Length' => filesize($filename)
	        ));
                 
                $response->setHeaders($headers); 
               /* $headers = new Headers();
                
                $response->getHeaders()->addHeaders(array(
                     'Content-type' => $mimetype,                    
                     'filename'=>$result,
                ));*/
                //var_dump($filename);die();
                

                return $response; 
            }else{
                $return['error'] = $translate('Błąd! Nie masz uprawnień do pobrania tego pliku');
            }
        }else{
            $return['error'] = $translate('Błąd! Wybrany plik nie istnieje');
        }
        
        
       
        
        return $return;
        
    }
    
}
