<?php

return array(
    'router' => array(
        'routes' => array(
            'download' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/download[/:action][/:id][/type/:type][/ext/:ext]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',                        
                        'type'   => '[a-z]+',
                        'ext'   => '[a-z]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Download\Controller\Download',
                        'action'     => 'download',
                        'type'       => '',
                        'ext'       => '',
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Download\Controller\Download' => 'Download\Controller\DownloadController',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'download' => __DIR__ . '/../view',
        ),
    ),
);
