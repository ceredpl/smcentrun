<?php

return array(
    'router' => array(
        'routes' => array(
            'page' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '[/action/:action][/type/:type][/year/:year][/perpage/:perpage][/page/:page][,:id][,:subjectid][,:mainid][,:footerid]',
                    'constraints' => array(
                        'title'      => '[a-zA-Z0-9]+',
                        'id'         => '[0-9]+',
                        'subjectid'  => '[0-9]+',
                        'mainid'     => '[0-9]+',
                        'footerid'   => '[0-9]+',
                        'action'     => '[a-zA-Z][a-zA-Z0-9]*',
                        'type'     => '[a-zA-Z][a-zA-Z0-9]*',
                        'page'       => '[0-9]+',
                        'perpage'    => '[0-9]+',
                        'year'       => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Page\Controller\Page',
                        'action'     => 'index',
                        'type'     => '',
                        'id'         => 0,
                        'subjectid'  => 0,
                        'mainid'     => -1,
                        'footerid'   => -1,
                        'title'      => 'a',
                        'page'       => '',
                        'perpage'    => '',
                        'year'       => '',
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Page\Controller\Page' => 'Page\Controller\PageController',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'page' => __DIR__ . '/../view',
        ),
    ),
);
