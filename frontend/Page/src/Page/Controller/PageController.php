<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Page\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Page\Form\Contact as ContactForm;
use Zend\Mail;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use DOMPDFModule\View\Model\PdfModel;



class PageController extends AbstractActionController
{
    private $PaginationSlideWidth = 3;
    private $RowsPerPage = 10;
    
    /**
     * Renders main page or redirects to login form (for not logged yet)
     * 
     * @return array
     */
    public function boxAction()
    {
        $return = array();
        $layout = $this->layout();
       
       
        
        
        $id = strip_tags($this->getEvent()->getRouteMatch()->getParam('id'));
         
        $contactSQL = "Select MailContact from boxy where stan=1 and IsContact=1 order by kolej asc limit 1";
        $contact = \Globals::executeDBQuerry($contactSQL);
        if(isset($contact) && $contact->count()>0)
        {
            $return['contact'] = $contact->current();
        }
        
        $phonesSQL = "Select Nazwa, Telefon from kontakty where stan=1 order by kolej asc";
        $phones = \Globals::executeDBQuerry($phonesSQL);
        if(isset($phones) && $phones->count()>0)
        {
            $return['phones'] = $phones;
        }
        
        $boxesQL = 'select Id, Nazwa from boxy where stan=1 and IsContact=0 order by kolej asc';
        $boxes = \Globals::executeDBQuerry($boxesQL); 
        if(isset($boxes) && $boxes->count()>0)
        {
            $return['boxes'] = $boxes;
        }
               
        $boxSQL = "Select Id, Nazwa, Opis from boxy where stan=1 and Id=".$id." order by kolej asc";
        $box = \Globals::executeDBQuerry($boxSQL);
        if(isset($box) && $box->count()>0)
        {
            $return['box'] = $box->current();
        }
        
        $return['id'] = $id;
        
        $photosSQL = 'select Id, File, Thumb, Description'.mb_strtoupper(\Globals::getLang()).' as Description from boxy_photo where State=1 and BoxId='.$id.' order by Ord asc'; 
        $photos = \Globals::executeDBQuerry($photosSQL);
        if(isset($photos) && $photos->count()>0){
            $return['photos'] = $photos;
        }
        
        return $return;
    }    
     
    public function newsAction(){
        $return = array();
        
        $id = strip_tags($this->getEvent()->getRouteMatch()->getParam('id'));
         
        $contactSQL = "Select MailContact from boxy where stan=1 and IsContact=1 order by kolej asc limit 1";
        $contact = \Globals::executeDBQuerry($contactSQL);
        if(isset($contact) && $contact->count()>0)
        {
            $return['contact'] = $contact->current();
        }
        
        $phonesSQL = "Select Nazwa, Telefon from kontakty where stan=1 order by kolej asc";
        $phones = \Globals::executeDBQuerry($phonesSQL);
        if(isset($phones) && $phones->count()>0)
        {
            $return['phones'] = $phones;
        }
        
        $boxesQL = 'select Id, Nazwa from boxy where stan=1 and IsContact=0 order by kolej asc';
        $boxes = \Globals::executeDBQuerry($boxesQL); 
        if(isset($boxes) && $boxes->count()>0)
        {
            $return['boxes'] = $boxes;
        }
        
        $newsesSQL = "Select Id, Nazwa, Opis from news where stan=1 order by kolej asc";
        $news = \Globals::executeDBQuerry($newsesSQL);
        if(isset($news) && $news->count()>0)
        {
            $return['news'] = $news;
        }
        
        $return['id'] = $id;
        
        return $return;
    }
    
    public function processcontactAction(){
        $viewModel = new \Zend\View\Model\ViewModel();
        $viewModel->setTerminal(true); //!!! wyłącza layout !!!
        $id = strip_tags($this->getEvent()->getRouteMatch()->getParam('id'));
        $response = $this->getResponse();
        $response->setStatusCode(200);  //!!! zatrzumuje rendering !!!
        
        $post=$this->getRequest()->getPost();
     
        
        if($this->getRequest()->isPost()){
            $dane=$this->getRequest()->getPost();           
            $Imie=$dane['Name'];           
            $Nazwisko=$dane['Surname'];           
            $Mail=$dane['Email'];
            $Telefon=$dane['Telefon'];
            $Instytucja=$dane['Instytucja'];           
            $Tresc=$dane['Info'];     
      
            
            $this->flashMessenger()->setNamespace('contactform')->addMessage($this->getRequest()->getPost());
          
              
          
                
                 $contentHtml = '<h3>Kontakt ze strony '.\Globals::getConfig()->siteurl.'</h3>'; //przygotować kontent w htmlu
                                                
              
       $contentHtml .='Imie i nazwisko: '.$Imie." ".$Nazwisko.'<br/>';   
       $contentHtml .='Mail: '.$Mail.'<br/>'; 
       $contentHtml .='Numer telefonu: '.$Telefon.'<br/>';  
       $contentHtml .=($Instytucja!=""?'Nazwa instytucji: '.$Instytucja.'<br/>':"");
       $contentHtml .='Tresc: '.$Tresc.'<br/>'; 
     
        
       
      

                $transport = new \Zend\Mail\Transport\Smtp();
                        $options = new \Zend\Mail\Transport\SmtpOptions(array(
                            'name' => \Globals::getConfig()->mailname,
                            'host' => \Globals::getConfig()->mailsmtp,
                            'port' => 587,
                            'connection_class' => 'plain',
                            'connection_config' => array(
                                'username' => \Globals::getConfig()->mailaddress,
                                'password' => \Globals::getConfig()->mailpass,
                                'ssl' => 'tls',
                            ),
                        ));
                        $transport->setOptions($options);
                        
                        $mail = new \Zend\Mail\Message();
                        $mail->setFrom(\Globals::getConfig()->mailaddress, \Globals::getConfig()->mailsender);
                        $mail->setSubject('Kontakt ze strony '.\Globals::getConfig()->frontendtitle);

                        $mail->setEncoding("UTF-8");
                        
                        

                        $content = new \Zend\Mime\Part($contentHtml);
                        $content->type = "text/html";
                        
                        $body = new \Zend\Mime\Message();
                        $body->setParts(array($content));//, $attachement));
$recipient='justyna.orysiak@senat.gov.pl';
                        $mail->setBody($body);
                        $mail->setTo($recipient);
                        
                        $transport->send($mail);

                        $this->flashMessenger()->setNamespace('contactinfo')->addMessage('Wysyłka zakończona powodzeniem. Skontaktujemy się najszybciej jak będzie to możliwe.');
               // var_dump($snd);die();
           }
        
                    
        
         return $this->redirect()->toRoute('page', array('id'=>'33', 'title'=>'kontakt'));
    } 
}
