<?php

namespace Page\Form;

use Zend\Form\Form;

class Contact extends Form
{
    
    
    public function __construct($name = null)
    {
       
        parent::__construct('contact');
        $this->setAttribute('method', 'post');
        
        
        
        $this->add(array(
            'name' => 'Name',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required', 
                'placeholder' => 'Imię',
                ),
            'options' => array(
                 'label' => '',
                'allow_remove' => false,
            ),
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
        )); 
        
        $this->add(array(
            'name' => 'Surname',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required', 
                'placeholder' => 'Nazwisko',
                ),
            'options' => array(
                 'label' => '',
                'allow_remove' => false,
            ),
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
        )); 
        
        $this->add(array(
            'name' => 'Email',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required', 
                'placeholder' => 'Adres e-mail:',
                ),
            'options' => array(
                 'label' => '',
                'allow_remove' => false,
            ),
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
        )); 
        
        $this->add(array(
            'name' => 'Telefon',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required', 
                'placeholder' => 'Telefon:',
                ),
            'options' => array(
                 'label' => '',
                'allow_remove' => false,
            ),
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
        )); 
        $this->add(array(
            'name' => 'Instytucja',
            'attributes' => array(
                'type' => 'text',               
                'placeholder' => 'Nazwa instytucji:',
                ),
            'options' => array(
                 'label' => '',
                'allow_remove' => false,
            ),           
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
        )); 
        
        $this->add(array(
            'name' => 'Info',
            'attributes' => array(
                'type' => 'textarea',
                'required' => 'required', 
                'placeholder' => 'Treść informacji:',
                ),
            'options' => array(
                 'label' => '',
                'allow_remove' => true,
            ),
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
        )); 
        
     
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'class'  => 'button',
                'value' => 'wyślij',
                'id' => 'submitbutton',
            ),
        ));
    }
    
}
