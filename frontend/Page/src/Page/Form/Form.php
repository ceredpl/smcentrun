<?php

namespace Page\Form;

use Zend\Form\Form;

class Form extends Form
{
    
    
    public function __construct($name = null)
    {
       
        parent::__construct('contact');
        $this->setAttribute('method', 'post');
        
        
        
        $this->add(array(
            'name' => 'ContactName',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required',
            ),
            'options' => array(
                'label' => 'Imię i nazwisko*:',
                'allow_remove' => false,
            ),
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
        )); 
        
        $this->add(array(
            'name' => 'Email',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required',
            ),
            'options' => array(
                'label' => 'Adres e-mail*:',
                'allow_remove' => false,
            ),
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'Content',
            'attributes' => array(
                'required' => 'required',
                'maxlength' => 300,
            ),
            'options' => array(
                'label' => 'Treść*:',
                'allow_remove' => true,
                'label_attributes' => array(
                    'class' => 'wide',
                ),
            ),
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
        ));
        
        
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Wyślij',
                'id' => 'submitbutton',
            ),
        ));
    }
    
}
