var siteurl = '';
var hostArray = window.location.host.split(".");

if(hostArray[0]=='www'){
        siteurl = 'http://'+window.location.host+'/';
}else{
        siteurl ='http://'+window.location.host+'/';
}
host = siteurl;
siteurl += 'centrum/public/cms/'; //usunąć po przeniesieniu !!!






$(document).ready(function(){

         /*
          * Zmiana stanu użytkownika
          */
        if((".change-state").length){
           $(".change-state").click(function(){
                tmpArr = $(this).parent().parent().attr("id").split("-");
                stan=0;
                if ($(this).parent().parent().find(".active").length>0){
                    stan=1;                   
                }
                $.post(siteurl+tmpArr[0]+'/changestate', {stateid:tmpArr[1], stan:stan}, function(data){
                    if(data.indexOf("1")==-1){
                        alert('Nie udało się zmienić stanu');
                    }else{
                        location.reload();
                    }					
                });
                return false;
            });
        }
       /*
          * Zmiana stanu użytkownika end
          */
   
    /*
          * Zmiana wartości
          */
        if((".change-value").length){
           $(".change-value").click(function(){
                tmpArr = $(this).attr("id").split("-");
                id = $(this).parent().parent().attr("id").split("-")[2];        
                if(id==undefined)
                {
                  id = $(this).parent().parent().attr("id").split("-")[1];        
                }
                
                $.post(siteurl+tmpArr[0]+'/changevalue', {id:id,stateid:tmpArr[2], value:tmpArr[1]}, function(data){
                    if(data.indexOf("1")==-1){
                        alert('Nie udało się zmienić stanu');
                    }else{
                        location.reload();
                    }					
                });
                return false;
            });
        }
       /*
          * Zmiana wartości end
          */
         
         /*
          * sortowanie
          */
         /*if($("#sortable-box").length){     
            $("#sortable-box, .sub").sortable({
               

                stop: function(event, ui){
                    var order = $(this).sortable('toArray');
                    id=$(".tree").attr("id");
                   // console.log(id);
                    $.post(siteurl+id+'/changeKolej', {'order': order},
                    function(data){                                                  
                    });
                  
                }
            });
        }
        */
        if($(".list-sortable").length){
            rootNode = $('.root').children("ul.list-sortable");           
            $(".list-sortable").sortable({
                placeholder: "ui-state-highlight",
                stop: function(event, ui){
                    var order = $(this).sortable('toArray');
                    id=$(".tree").attr("id");                    
                    $.post(siteurl+id+'/changeTurn', {'order': order},
                    function(data){                                                  
                    });                    
                }
            });
        }
        
        
        /*
         * sortowanie end
         */
        
        /*
         * Mapa google 
         */
        if($("#map").length)
        {
            var stylesArray = [
                 {
                "featureType": "landscape",
                "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 65
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 51
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 30
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 40
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "administrative.province",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "lightness": -25
                        },
                        {
                            "saturation": -100
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "hue": "#ffff00"
                        },
                        {
                            "lightness": -25
                        },
                        {
                            "saturation": -97
                        }
                    ]
                }
            ];
            geocoder = new google.maps.Geocoder();
            var markers = [];
            var infowindows = [];
            latitude = $(".latitude").val();
            longitude = $(".longitude").val();
            var myOptions = {
                zoom: 10,
                center: new google.maps.LatLng(latitude,longitude),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: stylesArray
            };
            var map = new google.maps.Map(document.getElementById('map'),myOptions);
            var znacznik = new google.maps.LatLng(latitude,longitude);
            marker = new google.maps.Marker({
                position: znacznik,
                map: map
            });
            markers[0]=marker;
            function placeMarker(location) {
		marker = new google.maps.Marker({
		  position: location,
		  map: map
		});
                console.log(location);
                if(markers.length>0){
                    markers[0].setMap(null);
                }
                markers[0]=marker;
		var lat=marker.getPosition().lat();//szerokość
		var lon=marker.getPosition().lng();//długość
                $(".latitude").val(lat);
                $(".longitude").val(lon);  
                var latlng = new google.maps.LatLng(lat, lon);
                 geocoder.geocode( {'latLng':latlng}, function(results, status) {
                     if(status == google.maps.GeocoderStatus.OK){
                        address = results[0].formatted_address;   
                        $(".address").val(address);
                     }
                });
            }
            google.maps.event.addListener(map, 'click', function(event) {
                placeMarker(event.latLng);
              });
            $(".address").change(function() {
                    address=$(this).val();
                    
                    geocoder.geocode( { 'address': address}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) { 
                        if(typeof results[0].geometry.location.B=== "undefined")
                        {
                            lat=results[0].geometry.location.k;
                            lon=results[0].geometry.location.D;                         
                        }
                        else if(typeof results[0].geometry.location.D=== "undefined")
                        {
                            lat=results[0].geometry.location.k;
                            lon=results[0].geometry.bounds.wa.k;                           
                        }	
                        else
                        {
                            lat=results[0].geometry.location.k;
                            lon=results[0].geometry.location.B;                          
                        }				  
                          $(".latitude").val(lat);
                          $(".longitude").val(lon);     
                          if(markers.length>0){
                              markers[0].setMap(null);
                          }
                          map.setCenter(results[0].geometry.location);
                          var marker = new google.maps.Marker({
                              map: map,
                              position: results[0].geometry.location
                          });
                          markers[0]=marker;
                        } else {
                              alert("Geocode was not successful for the following reason: " + status);
                        }
                    });
            });
        }
        /*
         * Mapa google end
         */
        
        /*
         * Zaznaczanie wszystkich uprawnień     
         */
        if($(".checkall").length){            
            $(".checkall").bind("click",function(){
                isChecked = $(this).is(":checked");
                id=$(this).attr("data-id");
                $("#uprawnienia-"+id).find(".uprawnienie").each(function (){
                    if(!isChecked){                      
                        $(this).prop("checked", false);
                    }
                    else{
                        $(this).prop("checked", true);
                    }
                });
            });
        }
        /*
         * Zaznaczanie wszystkich uprawnień  end   
         */
         
         /*
          * roziwjanie menu w tree
          */
         nazwasciezki = $(".tree").attr('id');
          if((".rozwin").length && nazwasciezki){
               
           $(".rozwin").click(function(){
              
                if($(this).hasClass("show")){             
                    $(this).next(".sub").hide(); 
                    $(this).removeClass("show");
                }else{
                    $(this).next(".sub").show();  
                    $(this).addClass("show");
                }
                tmpStr = '';
                $('.list-sortable:visible').each(function(){ 
                    tmpStr += $(this).attr('id').split("-")[1]+';';                    
                });	
                $.cookie(nazwasciezki, tmpStr, {path:'/'});              
            });
             var ciacho = $.cookie(nazwasciezki);
                if(ciacho!=null){       
                    tmpArr = ciacho.split(";");           
                    for(i=0;i<tmpArr.length;i++){console.log(ciacho);  
                        $("#box-"+nazwasciezki+"-"+tmpArr[i]).children(".rozwin").click();

                    }
                }
        }
         /*
          * rozwijanie menu w tree end
          */
         
        /*
         * Pokaż/ukryj uprawnienia
         */
        if((".uprawnienia").length){
           $( ".uprawnienia" ).bind( "click", function() {
               //.find("active").attr("id").split("-")[1]
               
             ob=$( "#content-wrapper" ).find( ".active" );
             
              if(ob.length!=0){
                    idAktywnej=ob.attr("id").split("-")[1];                 
                    $("#uprawnienia-"+idAktywnej+"").css({display:'none'});
                    $("#uprawnienia-"+idAktywnej+"").removeClass('active');
               }
               
               id=$(this).attr("id").split("-")[1];
               
              
               $("#uprawnienia-"+id+"").addClass('active');
                $("#uprawnienia-"+id+"").css({display:'table'});

            });
        }
        /*
         * Pokaż/ukryj uprawnienia end
         */
        
        /*
         * usuwanie rekordu z bazy
         */
        if((".delete").length){            
                $( ".delete" ).bind( "click", function() {
                        if(confirm("Czy napewno chcesz usunąć rekord?")){
                            array = $(this).attr("id").split("-");
                            $.post(siteurl+array[0]+'/delete', {id:array[1],table:array[0]}, function(data){                            
                                 $(parent).detach();
                                 location.reload();                             
                         });
                         return false;
                     }else{
                         return false;
                     }
                 });
        }
        /*
        * usuwanie rekordu z bazy end
        */
        
        /*
         * Sort Button action
         */
        if($("#filter").length)
        {
            $("#filter").bind("click", function(){
                if($(this).hasClass("active"))
                {
                    $(this).removeClass("active");
                    $("#filter-window").slideUp(300);
                }
                else
                {
                    $(this).addClass("active");
                    $("#filter-window").css({"top":50});
                    $("#filter-window").slideDown(300);
                }           
            });
            $("#close-filter").bind("click",function (){
                $("#filter").removeClass("active");
                $("#filter-window").slideUp(300);
            });
            $("#clear-filter").bind("click",function (){
                $('input[name="Search"]').val("");
                $('select[name="sort"]').val("");
                $("#filter").removeClass("active");
                $("#filter-window").slideUp(300,function(){
                   $("#searchform").submit();              
                    
                   //location.reload();
               });
            });
        }        
        /*
         * Sort Button action end
         */
        
        
        /*
         * FORM PICTURE PREVIEW
         */
         if($(".min-file").length){
            $(".min-file").mouseover(function(e){
                var href = $(this).find("a").attr("href");
                var extArr = href.split(".");
                var ext = extArr[(extArr.length>0?extArr.length-1:0)];

                if(ext=='jpg' || ext=='gif' || ext=='png' || ext=='jpeg'){
                    $("body").append('<div id="foto-preview" style="position: absolute; top:'+(e.pageY+5)+'px; left:'+(e.pageX+5)+'px; width:auto; height:auto; z-index:89;"><img src="'+href+'" style="max-width:300px; max-height:300px;" alt="preview" /></div>');
                }
            });
            $(".min-file").mousemove(function(evt){
                    $("#foto-preview").css("top", (evt.pageY+5)+'px');
                    $("#foto-preview").css("left", (evt.pageX+5)+'px');
            });
            $(".min-file").mouseout(function(){
                    $("#foto-preview").detach();
            });
         }
        /*
         * FORM PICTURE PREVIEW == end==
         */
        
         /*
         * GALLERY STATE
         */
        //if((".gallery-state").length){
            lock = 0;
            $(".gallery-state").on('click', function(){
                picid = $(this).parent().parent().attr("id").split("_")[1];
                tablename = $(this).parent().parent().parent().attr("id").split("-")[1];
                
                
                
                if(lock==0){
                    obj = $(this);
                    lock = 1;
                    
                    if($(this).hasClass('prime')){
                        $.post(siteurl+tablename+'/gallery', {'primevalue':1, 'id':picid}, function(data){
                            if(data.indexOf("1")==-1){
                                alert('Zmiana stanu zakończona niepowodzeniem. Spróbuj ponownie.');
                            }else{
                                $(".gallery-state.prime").html('<');
                                $("span.buttons").removeClass('prime');
                                $(obj).html('nieaktywny');
                                $(obj).parent().addClass('prime');
                            }
                            lock=0;
                        });
                    }else{
                        $.post(siteurl+tablename+'/gallery', {'changevalue':1, 'id':picid, 'state':($(this).hasClass('active')?'0':'1')}, function(data){
                            if(data.indexOf("1")==-1){
                                alert('Zmiana stanu zakończona niepowodzeniem. Spróbuj ponownie.');
                            }else{
                                if($(obj).hasClass('inactive')){
                                    $(obj).removeClass('inactive').addClass('active').html('aktywny');
                                }else if($(obj).hasClass('active')){
                                    $(obj).removeClass('active').addClass('inactive').html('nieaktywny');
                                }
                            }
                            lock=0;
                        });
                    }
                    
                    
                }
                return false;
            });
        //}
        /*
         * GALLERY STATE == end ==
         */
        /*
         * GALLERY DELETE
         */
        //if((".gallery-delete").length){
            dellock = 0;
            $(".gallery-delete").on('click', function(){
                if(confirm("Czy napewno chcesz wykonać tę akcję?")){
                    picid = $(this).parent().parent().attr("id").split("_")[1];
                    tablename = $(this).parent().parent().parent().attr("id").split("-")[1];

                    if(dellock==0){
                        obj = $(this);
                        dellock = 1;
                        $.post(siteurl+tablename+'/gallery', {'delete':1, 'id':picid}, function(data){
                            if(data.indexOf("1")==-1){
                                alert('Usunięcie zakończone niepowodzeniem. Spróbuj ponownie.');
                            }else{
                                $(obj).parent().parent().hide(500, function(){
                                    $(this).remove();
                                });
                            }
                            dellock = 0;
                        });
                    }
                    return false;
                }else{
                    return false;
                }
            });
        //}
        /*
         * GALLERY DELETE == end ==
         */
        
        /*
         * GALLERY SORTABLE
         */
        if($(".sortable").length){
            $(".sortable").sortable({
                stop: function(event, ui){
                    var order = $(this).sortable('toArray');
                    var idArr = $(".sortable").attr("id").split("-");
                    
                    $.post(siteurl+idArr[1]+'/gallery/sort/'+idArr[2], {'order': order},
                    function(data){
                            //alert(data);
                    });
                }
            });
        }
        /*
         * GALLERY SORTABLE == end ==
         */
        /*
         * GALLERY BOX
         */
        //if($("#gallery-box").length){
            $(".gallery-edit").on('click', function(e){
                id = $(this).parent().parent().attr('id').split("_")[1];
                $("#gallery-id").val(id);
                var left = e.pageX;     
                if (screen.width < left+350){
                    left = screen.width - 400;
                }
                
                route = $(this).parent().parent().parent().attr('id').split("-")[1];
                $.post(siteurl+route+'/gallery/description', {'id':id}, function(data){
                    $("#gallery-box").find("textarea").val(data);
                    $("#gallery-box").css({"left" : left+'px', "top" : (e.pageY-278)+'px'});
                    $("#gallery-box").show();
                    
                });
                
            });
            $("#gallery-box .greybutton").on('click', function(){
                $(this).parent().parent().hide();
                $("#gallery-id").val("");
            });
       // }
        //if($("#gallery-desc").length){
            $(".gallery-description").on('click', function(e){
                id = $(this).parent().parent().attr('id').split("_")[1];
                var left = e.pageX;     
                if (screen.width < left+350){
                    left = screen.width - 420;
                }
                
                route = $(this).parent().parent().parent().attr('id').split("-")[1];
                $.post(siteurl+route+'/gallery/description', {'id':id}, function(data){
                    tmpArr = data.split("\r\n");
                    $("#gallery-desc").find("p").html(tmpArr.join("<br/>"));
                    $("#gallery-desc").css({"left" : left+'px', "top" : (e.pageY+28)+'px'});
                    $("#gallery-desc").show();
                });
                
            });
            $("#gallery-desc .greybutton").on('click', function(){
                $(this).parent().hide();
                $("#gallery-desc").find("p").html('');
            });
        //}
        /*
         * GALLERY BOX == end ==
         */
        
    
        /*
         * FORM FILE DELETE
         */
         if((".del-file").length){
            $(".del-file").click(function(){
                if(confirm("Czy napewno chcesz wykonać tę akcję?")){
                    tmpArr = $(this).attr("id").split("-");
                    div = $(this).parent().children(".icon-file");
                    obj = $(this);

                    $.post(siteurl+tmpArr[1]+'/usunplik', {'delname': tmpArr[2], 'delid':tmpArr[3]}, function(data){
                        if(data!=1){
                            alert('Usunięcie zakończone niepowodzeniem. Spróbuj ponownie.');
                        }else{
                            $(div).detach();
                            $(obj).parent().detach();
                           // location.reload();
                        }
                    });
                    return false;
                }else{
                    return false;
                }
            });
             $(".datetimepicker").datetimepicker({
                timeText : 'Czas',
                hourText : 'Godzina',
                minuteText : 'Minuta',
                secondText : 'Sekundy',
                currentText : 'Teraz',
                dateFormat: 'yy-mm-dd',
                closeText : 'Zamknij'
            });
        }
        /*
         * FORM FILE DELETE == end ==
         */
        
        
       /*
         * wyswig
         */
       tinyMCE.init({
        // General options
        mode : "specific_textareas",
	editor_selector : "wyswig",
        language: "pl",
        theme : "advanced",
        skin : "o2k7",
        skin_variant : "silver",
        width: "950",
        height: "400",
        plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,visualblocks",

        // Theme options
        theme_advanced_buttons1 : "save,newdocument,bold,italic,underline,strikethrough,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect,cut,copy,paste,pastetext,pasteword,search,replace,bullist,numlist,outdent,indent,blockquote,,undo,redo,link",
        theme_advanced_buttons2 : ",unlink,anchor,image,cleanup,help,code,insertdate,inserttime,preview,forecolor,backcolor,tablecontrols,hr,removeformat,visualaid,sub,sup,charmap,emotions,iespell,media,advhr,print,ltr,rtl,fullscreen",
        theme_advanced_buttons3 : "insertlayer,moveforward,movebackward,absolute,styleprops,cite,abbr,acronym,del,ins,attribs,visualchars,nonbreaking,template,pagebreak,restoredraft,visualblocks",
    
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,
	theme_advanced_font_sizes: "10px,12px,13px,14px,16px,18px,20px,24px,28px,32px,36px,42px,60px",
        font_size_style_values: "10px,12px,13px,14px,16px,18px,20px,24px,28px,32px,36px,42px,60px",
        theme_advanced_fonts : 
                "Arial=arial,helvetica,sans-serif;"+
               
                "Book Antiqua=book antiqua,palatino;"+
              
                "Courier New=courier new,courier;"+
              
                "Helvetica=helvetica;"+
                "Impact=impact,chicago;"+
                
                "Tahoma=tahoma,arial,helvetica,sans-serif;"+
                
                "Times New Roman=times new roman,times;"+
                "Open sans=open sans,arial,helvetica,sans-serif;"+
                "Kingthings=kingthings,arial,sans-serif;"+               
                "Domyślna=arial,helvetica,sans-serif;"
              ,
        // Example content CSS (should be your site CSS)
        //content_css : "../css/style.css",
         
        
       file_browser_callback : MadFileBrowser,
        relative_urls : false,
        remove_script_host : false,



        // Style formats
        style_formats : [
                {title : 'Bold text', inline : 'b'},
                {title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
                {title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
                {title : 'Example 1', inline : 'span', classes : 'example1'},
                {title : 'Example 2', inline : 'span', classes : 'example2'},
                {title : 'Table styles'},
                {title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
        ]

});
function MadFileBrowser(field_name, url, type, win) {   
	  tinyMCE.activeEditor.windowManager.open({
              
	      file : siteurl+"/lib/tinymce/mfm.php?field=" + field_name + "&amp;url=" + url + "",
	      title : 'File Manager',
	      width : 800,
	      height : 450,
	      resizable : "no",
	      inline : "yes",
	      close_previous : "no"
	  }, {
	      window : win,
	      input : field_name
	  });
	  return false;
	}
        $(".datetimepicker").datetimepicker({
            timeText : 'Czas',
            hourText : 'Godzina',
            minuteText : 'Minuta',
            secondText : 'Sekundy',
            currentText : 'Teraz',
            dateFormat: 'yy-mm-dd',
            closeText : 'Zamknij'
        });
        $(".colorpicker").colorpicker();
    });
    $.webshims.setOptions('forms-ext', { 
        datepicker: { 
            changeMonth: true, 
            changeYear: true,
            dateFormat: 'yy-mm-dd'
        } 
    });
    $.webshims.polyfill();
     
        
    if($(".foto-gal").length){
           $(".foto-gal").prettyPhoto({social_tools:false});
    }
        
  $(window).scroll(scrollEvent);
       
       
function scrollEvent(){
    windowtop=$(window).scrollTop();
    header = $("nav").height();
    //console.log(janusz);
    if(windowtop>header)
    {
        $("nav").css({"height":"53"});     
        $("#home").addClass('small');  
        $(".menuUL").addClass('small');  
        $("#logout").addClass('small');  
        $("#lang-controls").addClass('small');  
    }else{
        $("nav").css({"height":"70"});
        $("#home").removeClass('small');  
        $(".menuUL").removeClass('small');  
        $("#logout").removeClass('small');  
        $("#lang-controls").removeClass('small');  
    }
};        
        
