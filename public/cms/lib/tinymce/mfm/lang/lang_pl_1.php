<?php
//ENGLISH
//for version 0.12

$lng['delete'] = 'Usuń';
$lng['delete_title'] = 'Usuń plik';

$lng['refresh'] = 'Odśwież';
$lng['refresh_files_title'] = 'Odśwież listę plików';
$lng['refresh_tree_title'] = 'Odśwież listę folderów';

$lng['new_folder'] = 'Nowy folder';
$lng['new_folder_title'] = 'Stwórz nowy folder';
$lng['delete_folder'] = 'Usuń folder';
$lng['delete_folder_title'] = 'Usuń bierzący folder';
$lng['default_folder'] = 'new_folder';

$lng['new_file'] = 'Dodaj plik';
$lng['new_file_title'] = 'Wyślij nowy plik';
$lng['new_file_manipulations'] = 'Edycja obrazka';

$lng['form_file'] = 'Plik:';
$lng['form_width'] = 'Zmień szerokość do:';
$lng['form_rotate'] = 'Obróć obrazek:';
$lng['form_greyscale'] = 'Przekształć na skalę szarości';
$lng['form_submit'] = 'Wyślij';

$lng['message_created_folder'] = ' - folder stworzony!'; //(after folder name)
$lng['message_cannot_create'] = 'Nie można utworzyć folderu '; //(before folder name)
$lng['message_cannot_write'] = 'Sprawdź uprawnienia!';
$lng['message_exists'] = 'Folder istnieje!';
$lng['message_cannot_delete_nonexist'] = 'Nie można usunąć pliku. Plik nie istnieje!';
$lng['message_cannot_delete'] = 'Nie można usunąć pliku!';
$lng['message_deleted'] = 'Plik skasowany!';
$lng['message_uploaded'] = 'Plik został wysłany pomyślnie!';
$lng['message_upload_failed'] = 'Błąd: Nie można wysłać pliku :(';
$lng['message_wrong_dir'] = 'Błąd: Folder nie istnieje!';
$lng['message_folder_deleted'] = 'Folder skasowany!';
$lng['message_cant_delete_folder'] = 'Błąd: Nie można usunąć folderu!';
$lng['message_folder_not_exist'] = 'Błąd: Nie ma takiego folderu!';

$lng['ask_folder_title'] = 'Nazwa folderu:';
$lng['ask_really_delete'] = 'Jesteś pewien?';

$lng['loading'] = 'Loading...';

$lng['window_title'] = 'File Manager';
?>