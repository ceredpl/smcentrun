$(document).ready(function()
{
		var X = new Array();
		var Y = new Array();
		var settings = {color: '#ff0000', stroke: 3};
		var offset = $("#map-div").offset();
		var l,r = 0;
		var yesno = false;
		
		if($("#coords").length && $("#coords").val()!='') //sprawdza czy istnieje dane id w DOM - jesli tak czy nie jest puste
		{
			var arrajs = $("#coords").val().split(';');
			var j=0;
			
			for (j=0; j<arrajs.length; j++)
			{
				var tempS = new String(arrajs[j]);
				var arraj = tempS.split(',');
				
				var tempX = new Array();
				var tempY = new Array();
				var i = 0;

				for(i=0; i<arraj.length; i++)
				{
					if(i>0)
					{
						if(i%2==0)
						{
							tempX.push(arraj[i]);
						}
						else{
							tempY.push(arraj[i]);
						}
					}
					else{
						tempX.push(arraj[i]);
					}
					
				}

				$("#map-div").drawPolygon(tempX,tempY,settings);
			} 
		}
		
	$("#map-div").click(function(evt)
	{
		l = parseInt(evt.pageX - offset.left);
		r = parseInt(evt.pageY - offset.top - 20);
		
		X.push(l);
		Y.push(r);

		$("#coords").val($("#coords").val()+''+l+','+r+',');
		$("#map-div").drawPolyline(X,Y, settings);
		
	});
	
	$("#map-div").dblclick(function(evt)
	{
		X.push(X[0]);
		Y.push(Y[0]);
		
		$("#map-div").drawPolyline(X,Y, settings);
		$("#coords").append(X[0]+','+Y[0]+'; ');
		
		
		
		X = new Array();
		Y = new Array();
				
	});
	
	$("#clear").click(function()
	{
		$("#map-div").clearPolyLine();
		$("#coords").val('');
	});	
	
	$("#clear-coords").click(function()
	{
		$("#coords").val('');
	});
	
	
		$(".mieszkanie").mouseover(function(e)
		{
			$("body").append('<div id="infoBox" style="position:absolute; top:'+e.pageY+'px; left:'+e.pageX+'px; width:200px; height:300px; background:#000; opacity:0.6;"');
		});	
			
		$(".mieszkanie").mousemove(function(ev)
		{
			
			$("#infoBox").css('top',ev.pageY);
			$("#infoBox").css('left',ev.pageX);
		});
	 
	
		$(".mieszkanie").mouseleave(function()
		{
			$("#infoBox").detach();
		});
	
});