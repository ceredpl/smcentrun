var siteurl = '';
var hostArray = window.location.host.split(".");

if(hostArray[0]=='www'){
        siteurl = 'http://'+window.location.host.substr(4)+'/';
}else{
        siteurl ='http://'+window.location.host+'/';
}

siteurl += 'bremo/public'; //usunąć po przeniesieniu !!!

$(document).ready(function(){
	
	var obj = $(this);
        /*
         * Otwieranie linku w nowym oknie
         */
        $(".blank").click(function(){
            window.open($(this).attr('href'),'_blank');
            
            return false;
        });
        /*
         * Otwieranie linku w nowym oknie end
         */
        
        
        /*
         * slider na stronie głównej
         */
        var licznik=0;
        var kierunek=0;
        
        var przesuniecie=100;
        var width = $('#slider-wrapper').width();
        var parentWidth = $('#slider-wrapper').offsetParent().width();
        var percent = 100*width/parentWidth;
        var szerokoscCalkowita=parseInt(percent)/100;
        var lock=0;
      
        if($("#slider-wrapper").length){
             $('#scroll-left').click(function () {
	if(licznik>0){
		$("#slider-wrapper").animate({"left": "+="+przesuniecie+"%"}, "slow");	 
                $("#button-"+licznik).removeClass('active');
		licznik--;
                $("#button-"+licznik).addClass('active');
		kierunek=1;
                }
        });
	
        $('#scroll-right').click(function () 
            {
                    if(licznik<szerokoscCalkowita-1){
                            $("#slider-wrapper").animate({"left": "-="+przesuniecie+"%"}, "slow");    
                            $("#button-"+licznik).removeClass('active');
                            licznik++;
                            $("#button-"+licznik).addClass('active');
                            kierunek=0;
                    }	
            });
            if($(".banner-slider").length>1){
                $.timer(function(){
                 
                        if(licznik==szerokoscCalkowita-1){
                                kierunek=1;
                        } else if(licznik==0){
                                kierunek=0;
                        }
                        if(kierunek==1){
                                $("#slider-wrapper").animate({"left": "+="+przesuniecie+"%"}, "slow");  
                                $("#button-"+licznik).removeClass('active');
                                licznik--;
                                $("#button-"+licznik).addClass('active');
                        }
                        if(kierunek==0){
                                $("#slider-wrapper").animate({"left": "-="+przesuniecie+"%"}, "slow");       
                                $("#button-"+licznik).removeClass('active');
                                licznik++;
                                $("#button-"+licznik).addClass('active');
                        }
                   
                }, 5000, true);
            }
	}
        $('.przycisk').click(function () 
	{
            id=$(this).attr("id").split("-")[1];
            procenty=parseInt(id)*100;           
            $("#slider-wrapper").animate({"left":"-"+procenty+"%"}, "slow");
            licznik=id;
            $("#przyciski").find('.active').removeClass('active');
             $("#button-"+licznik).addClass('active');
           
        }		
	);
         /*
         * slider na stronie głównej end
         */
        
        /*
         * zmiana ilości felg
         */
        $( ".ilosc input" ).change(function() {
            oldprice=$(this).parent().find('input[name="current_price"]').val();
            id=$(this).parent().find('input[name="current_price"]').attr("id").split("-")[1];
            ilosc=$(this).val();//nowa ilosc
            staracena=$("#ceanaallhidden-"+id).val();//stara cena
            newprice=ilosc*oldprice;//nowa cena
             oldrazem=$("input[name='cenarazem']").val();
            if(newprice<staracena)
                {
                    roznica=staracena-newprice;
                    tmp=oldrazem-roznica;
                    $("input[name='cenarazem']").val(tmp);
                    $("#razem").text("Razem: "+tmp+".00 PLN");
                }else{
                    roznica=newprice-staracena;                   
                    tmp=parseInt(oldrazem)+parseInt(roznica); 
                    $("input[name='cenarazem']").val(tmp);
                    $("#razem").text("Razem: "+tmp+".00 PLN");
                }
           
            $("#ceanaallhidden-"+id).val(newprice);  
            $("#cenacalkowita-"+id).text(newprice+".00 PLN");
           
        });
        /*
         * zmiana ilości felg end
         */
        $("input[name='radio']").change(function() {
                if($(this).is(":checked")) {
                    $(".hidden").show();
                }else{
                    $(".hidden").hide();
                }
                $('#textbox1').val($(this).is(':checked'));        
            });
        /*
         * Newsletter submit
         */
         $('#newsletter-form').on("submit", function (e) 
	{            
            mail=$(this).find('input[type=text]').val();
            e.preventDefault();
         
            $.post(siteurl+'/newsletter/mail', {'Email':mail}, function(data){
                
                    if(data.success){
                        $("#newsletter-message").show();
                        $("#newsletter-message").text(data.message);
                    }else{
                        console.log(data);
                        $("#newsletter-error").show();
                        $("#newsletter-error").text(data.error);
                    }			
                }, 'json');
        });
        /*
         * Newsletter submit end
         */
        
        
        $('#producenci').change( function(){      
            adres="http://localhost"+$(this).val();    
            if(adres!=-1){
                window.location.href = adres;    
            }
                             
        });

        /*
         * usuwanie produktu
         */
        $('.delete').click(function () 
	{
            id=$(this).attr("id").split("-")[1];
                $.post(siteurl+'/deletefromcart', {'id':id}, function(data){                
                   	location.reload(); 
                }, 'json');
        });

         /*
          * usuwanie produktu end
          */


        /*
         * rozwinięcie produktu
         */
         $('.produkth3').click(function () 
	{
            
                prevId=$("#leftContent").find(".active").attr("id").split("-")[1];
            
                if(prevId){
                    $("#opis-"+prevId).hide("slow");
                    $("#opis-"+prevId).removeClass("active");
                    $("#leftContent").find(".aktywny").removeClass("aktywny");
                }
                    id=$(this).attr("id").split("-")[1];
                    $("#opis-"+id).show("slow");                   
                    $("#opis-"+id).addClass("active");
                    $("#link-"+id).addClass("aktywny");
        });
        /*
         * rozwinięcie produktu end
         */
        
           /*
         * prettyPhoto
         */
        $(".photos a").prettyPhoto({social_tools:false});
        $("a.prettyPhoto").prettyPhoto({social_tools:false});
        /*
         * scroll top
         */
            $("#pageUp").click(function() {
                $("html, body").animate({ scrollTop: 0 }, "slow");
            return false;
          });
        /*
         * scroll top end
         */
});  
        
$.webshims.polyfill();
